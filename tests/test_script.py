from ada_processing.data_plotter import DataPlotter
import os

print(os.listdir(os.getcwd()))
print(os.listdir(os.path.join(os.getcwd(), 'test_data')))
print(os.listdir(os.path.join(os.getcwd(), 'test_data', '2019-08-22_10-47-11')))
path = [os.path.join(os.getcwd(), 'test_data', '2019-08-22_10-47-11')]

dp = DataPlotter(path)
dp.main()