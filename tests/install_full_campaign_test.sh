#!/usr/bin/env bash

pip3 install --user git+https://gitlab.com/ada-chem/ada_processing@master#egg=ada_processing
pip3 install --user --upgrade -r tests/test_requirements.txt
python3 tests/test_script.py

# Check exit status
if [[ $? = 0 ]]; then
    echo "Test Passed"
else
    echo "Test Failed: $?"
    exit 1
fi

