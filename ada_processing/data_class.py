import copy


class Data(object):
    """
    A class to wrap around the data when sending it between nodes. This lets us keep track of the metadata associated
    with various pieces of data. For example, when passing a Position folder downstream, we would like to know what its
    parent sample folder is, which we keep track of in self.parent
    """

    def __init__(self, name=None, data=None, parent=None, metadata: dict = None, ):
        """
        initializes the data object with the given attributes
        :param name: name of the data
        :param data: the value of the data (gets transformed at each node)
        :param parent: parent object of the data
        :param metadata: dictionary of extra metadata associated with the data
        """

        self.parent = parent
        self.name = name
        self.data = data
        self.metadata = metadata or {}

    @property
    def parent_name(self):
        """
        easily view the parent's name
        :return: the parent's name or none
        """
        try:
            return self.parent.name
        except AttributeError:
            return None

    def get_dict(self):
        """
        helper function to return human readable dictionary of data object
        :return: a dictionary representing the data object
        """
        return {
            'name': self.name,
            'parent': self.parent_name,
            'data': self.data,
            'metadata': self.metadata,
        }

    def get_data(self):
        """
        get a dictionary of the data object's attributes
        :return: dictionary of data objs attributes
        """
        return {
            'name': self.name,
            'parent': self.parent,
            'data': self.data,
            'metadata': self.metadata
        }

    def copy(self, **kwargs):
        """
        copies a Data object, overwriting any attributes with **kwargs
        :param kwargs: the attributes to overwrite
        :return: the new copied object
        """
        kwargs = {**self.get_data(), **kwargs,
                  **({'metadata': {**copy.deepcopy(self.metadata), **kwargs.get('metadata', {})}})}
        return Data(
            **kwargs)

    def get_parent_name_startswith(self, startswith: str) -> str:
        """
        get the parent name that starts with startswith, if it exists
        :param startswith: the start of the parent name string to get
        :return: the parent name or none
        """
        try:
            if self.parent.name.startswith(startswith):
                return self.parent.name
            else:
                return self.parent.get_parent_name_startswith(startswith)
        except AttributeError as e:
            # logger.debug(e)
            # logger.debug(f"Object does not have a {startswith} parent.")
            raise (e)
