from ada_processing.data_class import Data
from ada_processing.nodes.filter_nodes import MetaFilterNode

from consecution import Node, GroupByNode


class Pusher(MetaFilterNode):
    """
    input: item
    output: push
    """

    def process_item(self, item):

        self.push(item)


class NoMath(MetaFilterNode):
    """
    input: item
    output: push item, append item to varied param global state
    """

    def process_item(self, item):
        if item.metadata['varied']:
            varied_param_name = f"{item.name}_{item.metadata['units']}"
            self.global_state.varied_params[varied_param_name]['units'] = item.metadata['units']
            self.global_state.varied_params[varied_param_name]['col_name'] = f"{item.name}_{item.metadata['units']}"
            self.global_state.varied_params[varied_param_name]['name'] = item.name
        self.push(item)


class Aligner(GroupByNode):
    """
    aligns an item by grouping it by the the parent that starts with self.startswith
    """

    def key(self, item: Data):
        return item.get_parent_name_startswith(self.startswith)

    def process(self, batch: [Data]):
        for item in batch:
            self.push(item)


class Logger(Node):
    """
    input: some Data item
    output: print the Data's dictionary
    """

    def process(self, item: Data):
        print(item.get_dict())


class SpectraLabeller(Node):

    def process(self, item: Data):
        item_copy = item.copy(metadata={'previous_node': self.name})
        if '_uvvis' in item.metadata['name']:
            if "t-data" in item.metadata['name']:
                item_copy.metadata['r-t'] = 't'
            if "r-data" in item.metadata['name']:
                item_copy.metadata['r-t'] = 'r'
            if "blank" in item.metadata['name']:
                item_copy.metadata['blank'] = True
            else:
                item_copy.metadata['blank'] = False

            if 'dill' in item.metadata['name']:
                item_copy.metadata['filetype'] = 'dill'
            elif 'json' in item.metadata['name']:
                item_copy.metadata['filetype'] = 'json'
            elif 'csv' in item.metadata['name']:
                item_copy.metadata['filetype'] = 'csv'
            else:
                item_copy.metadata['filetype'] = 'unknown'
            self.push(item_copy)


class CreateVariedParamObjects(Node):

    def process(self, item: Data):
        dict = item.data
        item_copy = item.copy()
        item_copy.name = item.metadata['key_name']
        item_copy.data = dict['value']
        item_copy.metadata['units'] = dict['units']
        item_copy.metadata['varied'] = dict['varied']

        # the 'path' along the pipeline which this item will go down, right now either 'chemical' or 'no_math'
        # 'chemical' -> item will be converted to mols, 'no_math' -> item will be left in its original units
        item_copy.metadata['path'] = dict['type']
        if dict['varied']:
            if item_copy.name not in self.global_state.varied_params.keys():
                varied_param_name = f"{item_copy.name}_{item_copy.metadata['units']}"
                self.global_state.varied_params[varied_param_name] = {}
                self.global_state.varied_params[varied_param_name]['units'] = dict['units']
                self.global_state.varied_params[varied_param_name]['name'] = item_copy.name
        for key, val in dict['metadata'].items():
            item_copy.metadata[key] = val
        self.push(item_copy)
