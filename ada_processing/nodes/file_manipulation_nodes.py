from ada_processing.nodes.filter_nodes import MetaFilterNode
from ada_processing.data_class import Data
from consecution import Node

import json
import re
import os
import logging
import glob
import pandas as pd
import dill

logger = logging.getLogger(__name__)


class PushJSONFiles(MetaFilterNode):
    """
    pushes json files
    input: folder path
    push: json files with name as filter
    """

    def process_item(self, item: Data):
        base_path = item.data
        if not os.path.exists(base_path):
            logger.warning(f"path does not exist: {base_path}")

        pattern = "*.json"
        search_path = os.path.join(base_path, pattern)

        for p in sorted(glob.glob(search_path)):

            self.push(item.copy(
                parent=item,
                name=os.path.basename(p).strip('.json'),
                data=p,
                metadata={'previous_node': self.name}
            ))


class PushSubdirectories(MetaFilterNode):
    """
    pushes all the directories downstream
    input: folder path
    push: each subdirectory
    """

    def process_item(self, item: Data):
        logger.info(f"processing {item.name}")

        for dir in sorted(os.listdir(item.data)):

            self.push(item.copy(
                parent=item,
                name=dir,
                data=os.path.join(item.data, dir),
                metadata={'previous_node': self.name}
            ))


class FolderObjLabeller(Node):
    """
    takes the subdirectories of a folder and edits the metadata to label the object as a campaign folder, sample folder,
    position folder, or file.
    input: a campaign folder, sample folder, position folder, or file
    output: input object, but with the metadata changed to the appropriate metadata (e.g. campaign -> sample)
    """

    def process(self, item: Data):
        item_copy = item.copy(metadata={'previous_node': self.name})

        if item.metadata['c-s-p'] == 'c':
            item_copy.metadata['c-s-p'] = 's'

        elif item.metadata['c-s-p'] == 's':
            item_copy.metadata['c-s-p'] = 'p'

        elif item.metadata['c-s-p'] == 'p':
            item_copy.metadata['c-s-p'] = 'f'

        # if its a sample, check to see whether it has been found to be incomplete from the logs
        if item_copy.metadata['c-s-p'] == 's':
            if 'incomplete_samples' in item_copy.metadata:
                if 'sample_' in item_copy.name:
                    sample_str = re.search('[0-9]+', item_copy.name).group(0)
                    try:
                        sample_num = int(sample_str)
                        if sample_num in item_copy.metadata['incomplete_samples']:
                            logger.info(f'skipping incomplete sample: {item_copy.name}')
                            return
                    except ValueError:
                        logger.error(f"Failed to extract sample number from name: {item_copy.name}")



        if not os.path.isdir(item.data):
            item_copy.metadata['c-s-p'] = 'f'
            item_copy.metadata['name'] = item.name
            item_copy.metadata['file_name'] = item.name

            if '_conductivity' in item.name:
                item_copy.metadata['name'] = '_conductivity.csv'
                item_copy.metadata['file_name'] = item.name

            if 'jpg' in item.name:
                if not 'clean' in item.name:
                    item_copy.metadata['filetype'] = 'jpg'

        self.push(item_copy)


class LoadCSV(MetaFilterNode):
    """
    input: path to csv
    output: df made of csv
    """

    def process_item(self, item: Data):
        path = item.data
        df = pd.read_csv(open(path, 'r'))
        df['file_name'] = item.name

        # remove .csv from name

        item.name = item.name[:-4]
        item_copy = item.copy(
            data=df,
            name=f"{item.name}_dataframe",
            metadata={'filetype': 'csv', 'previous_node': self.name},
        )
        self.push(item_copy)


class LoadDill(MetaFilterNode):
    """
    input: path to dill file
    output: python obj
    """

    def process_item(self, item: Data):
        path = item.data
        obj = dill.load(open(path, 'br'))
        self.push(item.copy(
            name=f"{item.name}_object",
            metadata={'filetype': 'dill', 'previous_node': self.name},
            data=obj
        ))


class LoadJson(MetaFilterNode):
    """
    input: path to json file
    output: python dict of json file
    """

    def process_item(self, item: Data):
        path = item.data
        logger.info(f"Loading file: {path}")
        try:
            d = json.load(open(path, 'r'))
            self.push(item.copy(
                name=f"{item.name}_dict",
                data=d,
                metadata={'filetype': 'json', 'previous_node': self.name}
            ))
        except json.decoder.JSONDecodeError:
            logger.exception(f"Failed to load files: {path}")



class PushDictItems(MetaFilterNode):
    """
    input: python dict
    output: push each element of the dict one by one to be routed
    """

    def process_item(self, item):
        dict = item.data

        for key, value in dict.items():
            self.push(item.copy(
                data=value,
                metadata={'key_name': key}
            ))


class LoadImage(MetaFilterNode):

    # when inheriting `MetaFilterNode`, we override `process_item` instead of `process`
    def process_item(self, item):
        path = item.data

        self.push(item.copy(
            name=f"{item.name}",
            data=path,
            metadata={'filetype': 'jpg', 'previous_node': self.name}
        ))
