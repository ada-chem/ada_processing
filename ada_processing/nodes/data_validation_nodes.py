from ada_processing.data_class import Data
from ada_processing.nodes.filter_nodes import MetaFilterGroupByNode, MetaFilterNode
from consecution import Node, GroupByNode
from ada_toolbox.data_analysis_lib.validation_tools import Validation

import numpy as np
import pandas as pd
import os
import logging

logger = logging.getLogger(__name__)


class ValidateImage(MetaFilterNode):

    # when inheriting `MetaFilterNode`, we override `process_item` instead of `process`
    def process_item(self, item):

        Validation(channel='test', bot="Validator").photograph(image_directory=item.data)


class ValidateConductivity(MetaFilterNode):

    def process_item(self, item: Data):
        path = item.data
        df = pd.read_csv(open(path, 'r'))
        df['file_name'] = item.name

        Validation(channel='test', bot="Validator").conductivity_data(conductivity_file=item.data)

