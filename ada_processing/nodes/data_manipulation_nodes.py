from ada_processing.data_class import Data
from ada_processing.nodes.filter_nodes import MetaFilterGroupByNode, MetaFilterNode
from ada_processing.data_processor_helpers import list_mean_std

from ada_toolbox.analytical_lib import analytical_tools as m
from ada_toolbox.data_analysis_lib import image_analysis as img_analysis
from ada_toolbox.data_analysis_lib import conductivity_analysis as cond
from ada_toolbox.data_analysis_lib.validation_thresholds import number_valid_observations_lm_threshold

from consecution import Node, GroupByNode
import numpy as np
import pandas as pd
import logging
import re

# hack to stop the ada_imaging tensorflow session from corrupting the Phoenics optimiser
from tensorflow import __version__ as tf_version
if tf_version != '1.4.0':
    from ada_imaging.labelling import LabelImage

logger = logging.getLogger(__name__)


class ProcessImage(MetaFilterGroupByNode):

    def key(self, item: Data):
        return item.parent.name

    def process_batch(self, batch: [Data]):

        crack_ratings = []
        dewetting_ratings = []

        for item in batch:

            # if 'after' in item.name and '_0' in item.name:
            if 'after' in item.name:
                path_to_image = item.data

                try:
                    crack_model = LabelImage('crack')
                    crack_rating = crack_model.label_file(path_to_image)[0]
                    del crack_model
                except:
                    logger.error("Failed to label image with crack score")
                    crack_rating = -1
                finally:
                    crack_ratings.append(crack_rating)

                try:
                    dewetting_model = LabelImage('dewetting')
                    dewetting_rating = dewetting_model.label_file(path_to_image)[0]
                    del dewetting_model
                except:
                    logger.error("Failed to label image with dewetting score")
                    dewetting_rating = -1
                finally:
                    dewetting_ratings.append(dewetting_rating)

                brightness = img_analysis.brightness_detection(image_directory=path_to_image)
                blur = img_analysis.blur_detection(image_directory=path_to_image)

                number_images = len(self.global_state.images)

                self.global_state.images[number_images + 1] = {'sample': item.get_parent_name_startswith('sample'),
                                                               'path': path_to_image,
                                                               'dewetting': dewetting_rating,
                                                               'crack': crack_rating,
                                                               'blur': blur,
                                                               'brightness': brightness
                                                               }

        if len(batch) > 0:

            crack_mean, crack_std = list_mean_std(crack_ratings)
            crack_name = "crack_detection_score"
            crack_units = "Unitless"

            dewetting_mean, dewetting_std = list_mean_std(dewetting_ratings)
            dewetting_name = "dewetting_score"
            dewetting_units = "Unitless"

            if 'percentage_of_optical_defects' not in self.global_state.measured_params.keys():
                self.global_state.measured_params['crack_defects'] = {'units': crack_units,
                                                                      'mean_col_name': f"{crack_name}_mean_{crack_units}",
                                                                      'std_col_name': f"{crack_name}_std_{crack_units}",
                                                                      'name': "Crack Score"}

                # Dewetting
                self.global_state.measured_params['dewetting_defects'] = {'units': dewetting_units,
                                                                          'mean_col_name': f"{dewetting_name}_mean_{dewetting_units}",
                                                                          'std_col_name': f"{dewetting_name}_std_{dewetting_units}",
                                                                          'name': "Dewetting Score"}

            # Crack
            self.push(item.copy(
                data=crack_mean,
                name=f"{crack_name}_mean",
                metadata={'previous_node': self.name,
                          'units': crack_units,
                          'varied': False}
            ))

            self.push(item.copy(
                data=crack_std,
                name=f"{crack_name}_std",
                metadata={'previous_node': self.name,
                          'units': crack_units,
                          'varied': False}
            ))

            # Dewetting
            self.push(item.copy(
                data=dewetting_mean,
                name=f"{dewetting_name}_mean",
                metadata={'previous_node': self.name,
                          'units': dewetting_units,
                          'varied': False}
            ))

            self.push(item.copy(
                data=dewetting_std,
                name=f"{dewetting_name}_std",
                metadata={'previous_node': self.name,
                          'units': dewetting_units,
                          'varied': False}
            ))


class CalculateAbsorbance(MetaFilterGroupByNode):
    """
    input: transmission and reflection dataframes
    output: absorbance dataframe
    """

    def key(self, item: Data):
        return item.get_parent_name_startswith('pos')

    def process_batch(self, batch: [Data]):

        for item in batch:

            if item.metadata['r-t'] == 't':
                t_data = item.data

            if item.metadata['r-t'] == 'r':
                r_data = item.data

        try:
            a_data = m.calc_absorbance_from_t_and_r(t_data=t_data, r_data=r_data)
            a_data = a_data.rename(
                columns={'calibrated_signal': 'absorbance', 'calibrated_signal_smoothed': 'absorbance_smoothed'})

            self.push(batch[0].copy(
                data=a_data,
                name=f"{item.name}_a_data",
                metadata={'previous_node': self.name, 'units': '?'}
            ))

        except:
            if 'blank' not in self.name:
                logger.warning("Failed to process absorbance data. Most likely cause: no blank slide data available.")


class CalculateFilmAbsorbance(GroupByNode):
    """
    input: transmission and reflection dataframes
    output: absorbance dataframe
    """

    def begin(self):
        self.T_0 = self.global_state.T_0
        self.R_0 = self.global_state.R_0

    def key(self, item: Data):
        return item.get_parent_name_startswith('pos')

    def process(self, batch: [Data]):
        if len(batch) > 0:

            # Reflectance and transmission data for the film, not blank
            R_1 = None
            T_1 = None
            x_nm = None

            for item in batch:

                if item.metadata['r-t'] == 't':
                    if item.metadata['blank'] is True:

                        self.T_0 = item.data['calibrated_signal']
                        self.global_state.T_0 = item.data['calibrated_signal']

                    else:
                        T_1 = item.data['calibrated_signal']

                if item.metadata['r-t'] == 'r':
                    if item.metadata['blank'] is True:

                        self.R_0 = item.data['calibrated_signal']
                        self.global_state.R_0 = item.data['calibrated_signal']

                    else:
                        R_1 = item.data['calibrated_signal']

                x_nm = item.data['x_(nm)']

            try:
                if R_1 is None or T_1 is None or x_nm is None:
                    logger.error(f"Could not calculate film absorbance. "
                                 f"Ensure that there is transmission and reflectance data.\n"
                                 f"Reflectance: {R_1}\nTransmission: {T_1}\nX_nm: {x_nm}")

                    raise Exception(f"Could not calculate film absorbance. "
                                    f"Ensure that there is transmission and reflectance data.\n"
                                    f"Reflectance: {R_1}\nTransmission: {T_1}\nX_nm: {x_nm}")

                df = m.calculate_film_absorbance(self.R_0, self.T_0, R_1, T_1, x_nm)
                self.push(batch[0].copy(
                    data=df,
                    name=f"{batch[0].name}_absorbance",
                    metadata={'previous_node': self.name, 'units': '?'}
                ))

            except (TypeError, UnboundLocalError) as e:
                logger.exception(e)
                logger.exception("failed to CalculateFilmAbsorbance because missing value, either missing blank data or"
                                 " regular spectra data. Check sample 0 to see if blank or film data exists.")


class CalculateMols(MetaFilterNode):
    """
    init
    input: volume of varied param
    output: mol for varied param
    """

    def process_item(self, item: Data):

        val = item.data
        if val < 0.001:
            val = 0
        if item.metadata['units'] == 'g' or item.metadata['units'] == 'mg':
            try:
                density = [val for key, val in item.metadata.items() if 'density' in key][0]
                mol = m.mols_from_mg(val, item.metadata['concentration_mg_ml'],
                                     item.metadata['molar_weight_ mg'],
                                     density_g_ml=density)
            except Exception as e:
                logger.exception(e)
                logger.error(
                    f"Missing value encountered when calculating Mol Ratio for {item.name}. Check experiment parameters json for any 'null' values.")
                raise e
        else:
            raise Exception(
                "Received a chemical that is not in mg! If you would like to handle chemicals that aren't in"
                " mg, change the functionality of this code! Otherwise, it appears that a non-chemical made "
                "it to this node. Check the experiment parameters json to make sure all variables with the "
                "'type' chemical are indeed chemicals and in mg!")
        item_copy = item.copy(
            data=mol,
            metadata={'units': 'mols', 'previous_node': self.name}
        )
        if item.metadata['varied']:
            varied_param_name = f"{item_copy.name}_{item_copy.metadata['units']}"
            self.global_state.varied_params[varied_param_name] = {}
            self.global_state.varied_params[varied_param_name]['units'] = 'mols'
            self.global_state.varied_params[varied_param_name]['name'] = f"{item.name}"
            self.global_state.varied_params[varied_param_name]['col_name'] = f"{item.name}_mols"
        self.push(item_copy)


class SampleDispenseDataToDF(MetaFilterNode):
    """
    input: dict of _experiment_params.json
    output: DataFrame
    """

    def process_item(self, item: Data):
        try:
            dispense = item.data['campaign_dispensed_amounts']
            # get sample number from parent_name (sample folder)
            sample_num = int(re.search('[0-9]+', item.parent_name).group(0))

            lines = []
            for chem, data in dispense.items():
                for i, v in enumerate(data.get('req_vol_list', [])):
                    d = {}
                    d['sample'] = sample_num
                    d['chemical'] = chem
                    d['attempt'] = i + 1 # start attempt at 1
                    # convert all to uL
                    d['optimiser_requested'] = data['metadata']['requested_vol'] * 1000
                    d['requested_volume'] = v * 1000
                    d['pump_calibrated_volume'] = data.get('cal_vol_list')[i] * 1000
                    d['dispensed'] = data.get('disp_vol_list')[i] * 1000
                    lines.append(d)

            df = pd.DataFrame(lines)
            self.push(item.copy(
                data=df, parent=item.parent,
                metadata={'previous_node': self.name},
                )
            )
        except:
            logger.exception(f"Failed to load dispense data from experiment_params.json")


class DispenseDataToDF(MetaFilterNode):
    """
    input: dict of json dispense data
    output: DataFrame
    """

    def process_item(self, item: Data):

        if item.name == 'dispense_accuracy_dict':
            dfs = []
            try:
                for chem, values in item.data.items():
                    df = pd.DataFrame.from_dict(values, orient='columns')
                    # scales to uL
                    df = df * 1000
                    df['chemical'] = chem
                    dfs.append(df)

                df = pd.concat(dfs)
                self.global_state.dataframes[self.df_name] = df
            except:
                logger.exception(f"Failed to load dispense_accuracy data, check json format")


class GaussianLimfitMinimize(Node):
    """
    input: film absorbance data
    output: fit_result
    """

    def begin(self):
        self.default_gaussian = [m.add_gaussian_region(name='a', upper_bound=2.5, lower_bound=2.45)]
        self.default_gaussian = m.add_gaussian_to_region(gaussian_bounds_ev=self.default_gaussian, region_name='a',
                                                         a=1.1, b=4.2, c=0.2)
        # only 1 gaussian so take first gaussian region
        self.default_gaussian = self.default_gaussian[0]
        self.fit_params = m.get_fit_params(0, self.default_gaussian['init_gaussians'][0])
        self.num_gaussians = len(self.default_gaussian['init_gaussians'])

    def process(self, item: Data):
        x_range_ev = item.data['energy_(eV)'][(item.data['energy_(eV)'] < self.default_gaussian['bounds_ev'][0]) &
                                              (item.data['energy_(eV)'] > self.default_gaussian['bounds_ev'][1])]
        y_range_abs = item.data['log_A_f'][(item.data['energy_(eV)'] < self.default_gaussian['bounds_ev'][0]) &
                                           (item.data['energy_(eV)'] > self.default_gaussian['bounds_ev'][1])]
        kws = {
            'x': x_range_ev,
            'y': y_range_abs,
            'num_gaussians': self.num_gaussians
        }
        y_avg = np.average(np.array([y_range_abs]))
        fit_result = {'a': y_avg,
                      'b': None,  # Doesn't matter
                      'c': 1}
        # fit_result = m.lmfitminimize(func=m.gaussian_cost, fitparams=self.fit_params, kws=kws)
        self.push(
            item.copy(data=fit_result, parent=item.parent, metadata={'previous_node': self.name, 'units': 'Unitless'}))


class GaussianAreaUnderCurve(MetaFilterNode):
    """
    input: gaussian fitting
    output: area under gaussian fitting
    """

    def process_item(self, item: Data):
        # fit_result = list(item.data.values())
        # area = m.area_under_gaussian(float(fit_result[0]), float(fit_result[2]))
        height = item.data['a']
        self.push(item.copy(data=height, parent=item.parent,
                            metadata={'previous_node': self.name, 'type': 'measured', 'units': 'Unitless'}))


class CalculatePseudomobility(GroupByNode):
    """
    input: conductivity and gaussian roi for position
    output: pseudomobility for position
    """

    def key(self, item: Data):
        return item.get_parent_name_startswith('pos')

    def process(self, batch: [Data]):
        for item in batch:

            if item.metadata['previous_node'] == "pos_spectra_gauss_area":
                gauss_roi = item.data
            else:
                conductivity = item.data['slope']
        try:

            if gauss_roi <= 0:
                pseudomobility = 0

            elif conductivity <= 0:
                pseudomobility = 0

            else:
                pseudomobility = conductivity / gauss_roi
            self.push(Data(name='pseudomobility_position', data=pseudomobility, parent=batch[0].parent,
                           metadata={'previous_node': self.name, 'type': 'measured', 'units': 'Siemens'}))
        except Exception as e:
            logger.debug(e)


class LinearRegression(GroupByNode):
    """
    input: a dataframe with columns x_axis and y_axis (inside Data obj)
    output: dict of slope, intercept, p_val, r_val, and std_err (inside Data obj)
    NOTE: This is a GroupByNode in order to align items further down the pipe.
    """

    def key(self, item: Data):
        return item.get_parent_name_startswith('pos')

    def process(self, batch: [Data]):
        for item in batch:
            df = item.data

            # Remove saturated data points. Saturation thresholds are defined in the toolbox.
            df_rs = cond.remove_saturated_points(data=df)

            # Fit RANSAC robust linear model to the data. Minimum valid points defined in toolbox.
            r2, slope, intercept = cond.linear_regression_RANSAC(data=df_rs,
                                                                 x_axis="Voltage",
                                                                 y_axis="Current")

            conductance = {
                'slope': slope,
                'intercept': intercept,
                'r2': r2
            }

            self.push(item.copy(
                data=conductance,
                metadata={'previous_node': self.name, 'type': 'measured', 'units': 'Siemens'}
            ))
