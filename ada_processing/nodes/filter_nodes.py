from consecution import Node, GroupByNode
import logging
from ada_processing.data_class import Data

logger = logging.getLogger(__name__)


class MetaFilterNode(Node):
    """
    This node is a superclass which allows nodes to only process items which have certain key value pairs in their
    metadata. To instantiate this node, pass it CalculatePseudomobility('pos_calc_pseudomobility')
    """

    def process(self, item: Data):
        # to catch the case where you inherit a filter node, but do not wish to filter
        try:
            filter = self.filter
            dict_list = filter.items()
        except AttributeError:
            return self.process_item(item)

        # if we want to filter. assume we have keys and vals
        accept = True
        for filter_key, filter_val in dict_list:

            # check to see if item.metadata has key
            try:
                item_val = item.metadata[filter_key]
            except KeyError as e:
                logger.debug(e)
                # if it doesn't, return (reject the item)
                accept = False
                break

            # if it does have the key, check to see if it matches the val we want
            if filter_val != item_val:
                # if it doesn't match, reject it
                accept = False
                break

        # only accept the item if it wasn't rejected
        if accept:
            return self.process_item(item)

    def process_item(self, item):
        pass


class MetaFilterGroupByNode(GroupByNode):

    def process(self, batch: [Data]):
        if len(batch) > 0:
            return_batch = []
            for item in batch:

                processed = False
                # to catch the case where you inherit a filter node, but do not wish to filter
                try:
                    filter = self.filter
                    dict_list = filter.items()
                except AttributeError as e:
                    return_batch.append(item)
                    processed = True

                if not processed:
                    # if we want to filter. assume we have keys and vals
                    accept = True
                    for filter_key, filter_val in dict_list:

                        # check to see if item.metadata has key
                        try:
                            item_val = item.metadata[filter_key]
                        except KeyError as e:
                            logger.debug(e)
                            # if it doesn't, return (reject the item)
                            accept = False
                            break

                        # if it does have the key, check to see if it matches the val we want
                        if filter_val != item_val:
                            # if it doesn't match, reject it
                            accept = False
                            break

                    # only accept the item if it wasn't rejected
                    if accept:
                        return_batch.append(item)
            self.process_batch(return_batch)

    def process_batch(self, item):
        pass
