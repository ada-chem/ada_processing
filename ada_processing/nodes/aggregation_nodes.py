from ada_processing.nodes.filter_nodes import MetaFilterNode, MetaFilterGroupByNode
from ada_processing.data_class import Data
from ada_processing.data_processor_helpers import aggregate_column, list_mean_std, add_position_sample_campaign_columns
from consecution import Node, GroupByNode
import logging
import pandas as pd
import copy

logger = logging.getLogger(__name__)


# Consecution tutorial
# https://colab.research.google.com/drive/1UHgzh28Tw81eCXiq720d5qQ5xmb136UX

class AggregateSample(GroupByNode):
    """
    This class groups data objects by sample. It is then used by other classes to overwrite the key method.
    This class is never used directly, it is used to overwrite the GroupByNode Key, and the class that inherits it
    overwrites the process method.

    input: item, Data class instance
    output: overwrites the key method of whichever class inherits it
    """

    def key(self, item: Data):
        # this returns the name of the sample associated with the item
        return item.get_parent_name_startswith('sample')


class AggregateSampleConductivity(AggregateSample):
    """
    Take in the conductivity data for a given sample and return the maximum conductivity recorded

    input: a batch of positional conductivity data
    output: max slope of sample
    """

    def process(self, batch: [Data]):

        # Conductivity df key, data class object items, by sample
        cond_dict = {}

        for item in batch:

            if item.name in cond_dict.keys():
                cond_dict[item.name].append(item)

            else:
                cond_dict[item.name] = []
                cond_dict[item.name].append(item)

        for key, val in cond_dict.items():

            # sample_conductance = []
            #
            # for position in val:
            #     pos_conductance = position.data
            #     sample_conductance.append(pos_conductance['slope'])
            #
            # max_conductance = max(sample_conductance)

            avg_slope, std_slope = aggregate_column(val, 'slope')
            units = batch[0].metadata['units']

            if key not in self.global_state.measured_params.keys():
                self.global_state.measured_params[key] = {'units': units, 'mean_col_name': f"{key}_mean_{units}",
                                                          'std_col_name': f"{key}_std_{units}", 'name': 'Conductance'}

            self.push(
                batch[0].copy(name=f"{key}_mean", parent=batch[0].parent.parent, data=avg_slope,
                              metadata={'method': 'mean', 'name': 'conductivity', 'type': 'measured',
                                        'previous_node': self.name, 'varied': False, 'units': units}))

            self.push(
                batch[0].copy(name=f"{key}_std", parent=batch[0].parent.parent, data=std_slope,
                              metadata={'method': 'std', 'name': 'conductivity', 'type': 'measured',
                                        'previous_node': self.name, 'varied': False, 'units': units}))


class AggregateSamplePseudomobility(AggregateSample):
    """
    input: a batch of positional pseudomobility values
    output: avg pseudomobility and std of pseudomobility
    """

    def process(self, batch):
        if len(batch) > 0:
            list = []
            for item in batch:
                list.append(item.data)
            avg, std = list_mean_std(list)
            units = batch[0].metadata['units']
            name = 'pseudomobility_sample'
            self.global_state.measured_params['pseudomobility'] = {'units': units,
                                                                   'mean_col_name': f"{name}_mean_{units}",
                                                                   'std_col_name': f"{name}_std_{units}",
                                                                   'name': "Pseudomobility"}
            self.push(
                Data(name=f"{name}_mean", parent=batch[0].parent.parent, data=avg,
                     metadata={'method': 'mean', 'name': 'pseudomobility', 'type': 'measured',
                               'previous_node': self.name, 'units': units, 'varied': False}))
            self.push(
                Data(name=f"{name}_std", parent=batch[0].parent.parent, data=std,
                     metadata={'method': 'std', 'name': 'pseudomobility', 'type': 'measured',
                               'previous_node': self.name, 'units': units, 'varied': False}))


class AggregateSampleGaussROI(AggregateSample):
    """
    input: a batch of positional gaussian ROI values
    output: avg gaussian roi and std of gauss roi
    """

    def process(self, batch):
        if len(batch) > 0:
            list = []
            for item in batch:
                list.append(item.data)
            avg, std = list_mean_std(list)
            units = batch[0].metadata['units']
            name = 'height_of_gauss'
            self.global_state.measured_params['gaussian_roi'] = {'units': units,
                                                                 'mean_col_name': f"{name}_mean_{units}",
                                                                 'std_col_name': f"{name}_std_{units}",
                                                                 'name': 'Height Of Gaussian'}
            self.push(
                Data(name=f"{name}_mean", parent=batch[0].parent.parent, data=avg,
                     metadata={'method': 'mean', 'name': 'area_under_gauss', 'type': 'measured',
                               'previous_node': self.name, 'units': units, 'varied': False}))
            self.push(
                Data(name=f"{name}_std", parent=batch[0].parent.parent, data=std,
                     metadata={'method': 'std', 'name': 'area_under_gauss', 'type': 'measured',
                               'previous_node': self.name, 'units': units, 'varied': False}))


class MergeAndAggSampleData(MetaFilterGroupByNode):
    """
    aggregates all measured and varied params at the sample level (e.g. mean and std of measured)
    """

    def key(self, item: Data):
        return item.get_parent_name_startswith('sample')

    def begin(self):
        self.item = Data(name='all_sample_data', parent=None, data=None)
        self.series_list = []
        self.len = 0

    def process_batch(self, batch: [Data]):
        data = pd.Series()
        data = add_position_sample_campaign_columns(data, batch[0])
        for item in batch:
            data[f"{item.name}_{item.metadata['units']}"] = item.data
            if item.metadata['varied']:
                self.global_state.varied_params[f"{item.name}_{item.metadata['units']}"]['col_name'] = f"{item.name}_{item.metadata['units']}"
            self.item.metadata[item.name] = item.metadata
        self.series_list.append(data.to_frame().transpose())

    def end(self):
        df = pd.concat(self.series_list, axis=0, sort=False, ignore_index=True)
        df['sample'] = df['sample'].astype(int)
        if 'campaign' in df.columns:
            df['campaign'] = df['campaign'].astype(int)
        self.global_state.dataframes['processed_sample'] = df
        self.global_state.metadata['processed_sample'] = self.item.metadata
        self.push(self.item.copy(data=df, metadata={'previous_node': self.name}))


class MergeAndAggPositionData(MetaFilterGroupByNode):
    """
    concatenates together dataframes from position level
    """

    def key(self, item: Data):
        return item.get_parent_name_startswith('pos')

    def begin(self):
        self.df_list = []
        self.item = Data(name='all_position_data')

    def process_batch(self, batch: [Data]):
        data = pd.Series()
        for item in batch:
            data = add_position_sample_campaign_columns(data, item)
            if item.metadata['previous_node'] == 'pos_conductivity_linregress':
                for key, val in item.data.items():
                    data.at[f"{item.name}_{key}"] = item.data[key]
            else:
                data.at[f"{item.name}_{item.metadata['units']}"] = item.data
            self.item.metadata[item.name] = item.metadata
        self.df_list.append(data.to_frame().transpose())

    def end(self):
        if len(self.df_list) > 0:
            df = pd.concat(self.df_list, axis=self.axis, ignore_index=True, sort=False)
            df['position'] = df['position'].astype(int)
            df['sample'] = df['sample'].astype(int)
            if 'campaign' in df.columns:
                df['campaign'] = df['campaign'].astype(int)
            if 'area_under_gauss_area' in df.columns:
                df['area_under_gauss_area'] = df['area_under_gauss_area'].astype(float)
            if self.transpose:
                df = df.transpose()
            self.global_state.dataframes[self.df_name] = df
            self.global_state.metadata[self.df_name] = self.item.metadata
            self.push(self.item.copy(data=df, metadata={'previous_node': self.name}))


class AggregateRawConductivity(MetaFilterNode):

    def begin(self):
        # create a list to store the conductivity dataframes as they enter the node
        self.df_list = []

        # create an item to store the metadata of all the items that enter the node
        self.item = Data(name='all_position_data')

    def process_item(self, item: Data):

        # create a copy of the item so we don't edit the original item
        item_data_copy = copy.copy(item.data)

        # add the campaign, sample, and position values to the conductivity dataframe
        item_data_copy = add_position_sample_campaign_columns(item_data_copy, item)

        # store the item's metadata
        self.item.metadata = item.metadata

        # store the dataframe
        self.df_list.append(item_data_copy)

    def end(self):

        # if there are any conductivity dataframes
        if len(self.df_list) > 0:

            # concat all the conductivity dataframes into one dataframe
            df = pd.concat(self.df_list, axis=self.axis, ignore_index=True, sort=False)

            # transpose the dataframe if necessary
            if self.transpose:
                df = df.transpose()

            # store the dataframe in the global state
            self.global_state.dataframes[self.df_name] = df

            # store the metadata in the global state
            self.global_state.metadata[self.df_name] = self.item.metadata

            # push the dataframe downstream
            self.push(self.item.copy(data=df, metadata={'previous_node': self.name}))


class AggregateDispenseData(MetaFilterNode):

    def begin(self):

        # create lists to store the different spectra dataframes
        self.df_list = []

        # create an item to store the metadata
        self.item = Data(name='all_sample_dispense_data')

    def process_item(self, item: Data):

        # create a copy of the item so that the original item isn't edited
        item_data_copy = copy.copy(item.data)

        # store the item's metadata
        self.item.metadata = item.metadata

        self.df_list.append(item_data_copy)

    def end(self):

        # if there are any dataframes
        if len(self.df_list) > 0:
            # concat the different lists into singular dataframes
            df = pd.concat(self.df_list, axis=self.axis, ignore_index=True, sort=False)

            # add df to the global state
            self.global_state.dataframes[self.df_name] = df

            # add metadata to the global state
            self.global_state.metadata[self.df_name] = self.item.metadata
            self.push(self.item.copy(data=df, metadata={'previous_node': self.name}))


class AggregateRawSpectra(MetaFilterNode):

    def begin(self):

        # create lists to store the different spectra dataframes
        self.df_list = []
        self.absorbance_df_list = []
        self.blank_absorbance_df_list = []
        self.film_absorbance_df_list = []

        # create an item to store the metadata
        self.item = Data(name='all_position_data')

    def process_item(self, item: Data):

        # create a copy of the item so that the original item isn't edited
        item_data_copy = copy.copy(item.data)

        # add the campaign, sample, and position columns to the spectra dataframe
        item_data_copy = add_position_sample_campaign_columns(item_data_copy, item)

        # store the item's metadata
        self.item.metadata = item.metadata

        # if the item is raw spectra, add the 'r-t' (reflectance or transmission) and 'blank' (blank slide or not) columns
        if item.metadata['previous_node'] == 'pos_spectra_load_dill':
            item_data_copy['r-t'] = item.metadata['r-t']
            item_data_copy['blank'] = item.metadata['blank']

        # if the item is absorbance, add it to the absorbance list
        if item.metadata['previous_node'] == 'pos_spectra_absorbance':
            self.absorbance_df_list.append(item_data_copy)
        elif item.metadata['previous_node'] == 'pos_spectra_absorbance_blank':
            self.blank_absorbance_df_list.append(item_data_copy)
        # if the item is film absorbance, add it to the film absorbance list
        elif item.metadata['previous_node'] == 'pos_spectra_film_absorbance':
            self.film_absorbance_df_list.append(item_data_copy)
        # otherwise, add it to the generic list
        else:
            self.df_list.append(item_data_copy)

    def end(self):

        # if there are any dataframes
        if len(self.df_list) > 0:
            # concat the different lists into singular dataframes
            df1 = pd.concat(self.df_list, axis=self.axis, ignore_index=True, sort=False)
            absorbance_df = pd.concat(self.absorbance_df_list, axis=self.axis, ignore_index=True, sort=False)
            if len(self.blank_absorbance_df_list) > 0:
                blank_absorbance_df = pd.concat(self.blank_absorbance_df_list, axis=self.axis, ignore_index=True,
                                                sort=False)
            df3 = pd.concat(self.film_absorbance_df_list, axis=self.axis, ignore_index=True, sort=False)

            # attempt to merge the dataframes including the campaign column. if this fails, don't include campaign to be merged on
            try:
                if len(self.blank_absorbance_df_list) > 0:
                    df_m1 = pd.merge(df1[df1['blank'] == False], absorbance_df,
                                     on=['x_(nm)', 'position', 'sample', 'campaign'])
                    df_m2 = pd.merge(df1[df1['blank'] == True], blank_absorbance_df,
                                     on=['x_(nm)', 'position', 'sample', 'campaign'])
                    df = pd.concat([df_m1, df_m2], axis=self.axis, ignore_index=True, sort=False)
                else:
                    df = pd.merge(df1[df1['blank'] == False], absorbance_df,
                                  on=['x_(nm)', 'position', 'sample', 'campaign'])
                df = pd.merge(df, df3, on=['x_(nm)', 'position', 'sample', 'campaign'])
            except KeyError:
                if len(self.blank_absorbance_df_list) > 0:
                    df_m1 = pd.merge(df1[df1['blank'] == False], absorbance_df, on=['x_(nm)', 'position', 'sample'])
                    df_m2 = pd.merge(df1[df1['blank'] == True], blank_absorbance_df,
                                     on=['x_(nm)', 'position', 'sample'])
                    df = pd.concat([df_m1, df_m2], axis=self.axis, ignore_index=True, sort=False)
                else:
                    df = pd.merge(df1[df1['blank'] == False], absorbance_df, on=['x_(nm)', 'position', 'sample'])
                df = pd.merge(df, df3, on=['x_(nm)', 'position', 'sample'])

            # if df needs to be transposed, transpose it
            if self.transpose:
                df = df.transpose()

            # add df to the global state
            self.global_state.dataframes[self.df_name] = df

            # add metadata to the global state
            self.global_state.metadata[self.df_name] = self.item.metadata
            self.push(self.item.copy(data=df, metadata={'previous_node': self.name}))
