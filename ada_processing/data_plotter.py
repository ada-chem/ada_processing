import copy
import json
import gc
import logging
from typing import List
import os

# from project
from ada_processing.data_processor import DataProcessor
from ada_processing.configuration.spectra_config import spectra_config
from ada_processing.data_processor_helpers import setup_logging
from ada_toolbox.matplotlib_lib import matplotlib_tools as mpl_pltr

from ada_toolbox.data_analysis_lib import conductivity_analysis as cond

import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

import plotly
from ada_toolbox.plotly_lib import plotly_tools as plotly_pltr

import warnings
import urllib
import re

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

plt.rcParams.update({'figure.max_open_warning': 0})
warnings.filterwarnings("ignore", module="ada_toolbox")


def slugify(text):
    text = urllib.parse.quote_plus(text)
    return re.sub(r'[\W_]+', '_', text)


class DataPlotter:

    def __init__(self,
                 paths_to_campaigns: List[str] = None,
                 path_to_zb_results_campaign_folder=None,
                 skip_specra = False,
                 **processor_kwargs,
                 ):

        # logging
        setup_logging()
        self.logger = logging.getLogger(__name__)

        if paths_to_campaigns is not None and path_to_zb_results_campaign_folder is not None:
            raise Exception('cannot pass both paths_to_campaigns and path_to_zb_results_campaign_folder')

        # handle the case where a campaign folder is passed
        if paths_to_campaigns is not None:
            # store the paths to the campaigns

            # create data processor instance and process campaign
            self.dp = DataProcessor()
            self.dp.process_campaigns(paths_to_campaigns, **processor_kwargs)

            # get the campaign name
            self.campaign_name = os.path.basename(paths_to_campaigns[0])

            # get the dataframes from the data processor
            self.skip_spectra = skip_specra

            self.spectra_df = None
            if not self.skip_spectra:
                self.spectra_df = self.dp.get_raw_spec_dataframe()
            self.conductance_df = self.dp.get_raw_cond_dataframe()
            self.position_df = self.dp.get_processed_position_dataframe()
            self.sample_df = self.dp.get_processed_sample_dataframe()
            self.dispense_df = self.dp.get_dispense_dataframe()
            self.sample_dispense_df = self.dp.get_sample_dispense_dataframe()

            # get the varied and measured parameters info
            self.varied_params = self.dp.get_varied_params()
            self.measured_params = self.dp.get_measured_params()
            self.images = self.dp.get_images()

        # handle the case where a zb-results folder is passed
        elif path_to_zb_results_campaign_folder is not None:

            # get the campaign name
            self.campaign_name = os.path.basename(path_to_zb_results_campaign_folder)

            # get the path to the processed data folder for this campaign
            processed_data_path = os.path.join(path_to_zb_results_campaign_folder, 'processed_data')

            # load all the dataframes in from the csvs
            self.spectra_df = pd.read_csv(os.path.join(processed_data_path, f"{self.campaign_name}_raw_spec.csv"))
            self.conductance_df = pd.read_csv(os.path.join(processed_data_path, f"{self.campaign_name}_raw_cond.csv"))
            self.position_df = pd.read_csv(
                os.path.join(processed_data_path, f"{self.campaign_name}_processed_positional.csv"))
            self.sample_df = pd.read_csv(
                os.path.join(processed_data_path, f"{self.campaign_name}_processed_sample.csv"))


            # load in the varied and measured parameters info
            with open(os.path.join(processed_data_path, f"{self.campaign_name}_varied_params.json"), 'r') as fp:
                self.varied_params = json.load(fp)
            with open(os.path.join(processed_data_path, f"{self.campaign_name}_measured_params.json"), 'r') as fp:
                self.measured_params = json.load(fp)
            with open(os.path.join(processed_data_path, f"{self.campaign_name}_images.json"), 'r') as fp:
                self.images = json.load(fp)

        else:
            raise Exception('must pass either path_to_zb_results_campaign_folder or paths_to_campaigns')

        # make results directory (should already exist)
        self.zb_results_directory = os.path.join(os.getcwd(), 'zb-results')
        try:
            os.makedirs(self.zb_results_directory, exist_ok=True)
        except OSError as e:
            self.logger.warning(e)
            pass

        # Make the campaign directory (should already exist)
        self.campaign_directory = os.path.join(self.zb_results_directory, self.campaign_name)
        try:
            os.makedirs(self.campaign_directory, exist_ok=True)
        except OSError as e:
            self.logger.warning(e)
            pass

        # Make the report, plots, processed data dirs
        self.matplotlib_plot_directory = os.path.join(self.campaign_directory, "matplotlib_plots")
        try:
            os.makedirs(self.matplotlib_plot_directory, exist_ok=True)
        except OSError as e:
            self.logger.warning(e)
            pass

        self.plotly_directory = os.path.join(self.campaign_directory, "plotly_plots")
        try:
            os.makedirs(self.plotly_directory, exist_ok=True)
        except OSError as e:
            self.logger.warning(e)
            pass

        self.pdf_path = os.path.join(self.campaign_directory, "matplotlib_reports")
        try:
            os.makedirs(self.pdf_path, exist_ok=True)
        except OSError as e:
            self.logger.warning(e)
            pass

        self.full_path = os.path.join(self.pdf_path, f"{self.campaign_name}_report.pdf")
        self.pdf = PdfPages(self.full_path)
        self.pdf_page_count = 0

        # set up attributes used later
        self.sample_position_df = None

    def get_sample_and_positional_dataframe(self):
        """
        uses self.sample_df and self.position_df
        :return: dataframe with both sample and positional information
        """
        # merge the sample and positional dataframes
        return pd.merge(self.sample_df, self.position_df, on=['sample', 'campaign'])

    def populate_mol_ratio_columns(self, dataframe, denominator_col):
        """
        add columns col / denom_col for each column with 'mols' in header
        :return: dataframe
        """
        # create a new dictionary so that we aren't editing the old one while iterating through it
        new_varied_params = copy.copy(self.varied_params)

        # iterate through the varied params
        for key, val in self.varied_params.items():
            # get the column name of the param
            col = val['col_name']

            # if that column is in mols:
            if '_mols' in col:

                # and that column is not the denominator column:
                if col != denominator_col:
                    # create a new column which is the mol ratio of the current varied param divided by the denominator
                    # column
                    dataframe[f'{col}_over_{denominator_col}'] = dataframe[col] / dataframe[denominator_col]

                    # add the new (mol ratio) column to the new varied params dictionary
                    new_varied_params[f'{col}_over_{denominator_col}'] = {'units': "mol ratio",
                                                                          'col_name': f'{col}_over_{denominator_col}',
                                                                          'name': f'{key.replace("_", " ")}'}

                    # remove the old (mols) column from the varied params
                    new_varied_params.pop(key)

        # set self.varied_params to be the new varied params
        self.varied_params = new_varied_params

        # return the dataframe with the mol ratio columns
        return dataframe

    @staticmethod
    def get_col_names_with_string(dataframe, string):
        """
        gets a list of the column names in given dataframe which contain the given string
        :param dataframe: the given dataframe
        :param string: the given string
        :return:
        """
        return_list = []
        for col in dataframe.columns:
            if string in col:
                return_list.append(col)
        return return_list

    @staticmethod
    def get_measured_and_varied_param_labels(measured_params=None, varied_params=None):
        """
        uses the varied and measured parameters to create a dictionary of labels of column names to nicely formatted strings
        :param measured_params: dictionary of measured parameters
        :param varied_params: dictionary of varied parameters
        :return: dictionary of labels
        """
        # create a dictionary to store the labels
        labels = {}

        # format the measured params if they exist
        if measured_params is not None:
            for key, val in measured_params.items():

                # if the param has a nicely formatted name, use that name and italicize it
                if 'name' in val.keys():
                    # if there are spaces in the string, italicize each section then combine them
                    if ' ' in val['units']:
                        new_string = '$\it{(}'
                        for string in val['units'].split():
                            new_string += "\it{" + string + "}\ "
                        new_string = new_string[:-2]
                        new_string += '\it{)}$'
                        labels[val['mean_col_name']] = val['name'].title() + " " + new_string

                    # otherwise, itilicize the whole string
                    else:
                        labels[val['mean_col_name']] = val['name'].title() + " $\it{(" + val['units'] + ")}$"

                # otherwise, use the column name
                else:
                    labels[val['mean_col_name']] = val['mean_col_name']

        # format the varied params if they exist
        if varied_params is not None:
            for key_a, val_a in varied_params.items():

                # if the param has a nicely formatted name, use that name and italicize it
                if 'name' in val_a.keys():

                    # if there are spaces in the string, italicize each section then combine them
                    if ' ' in val_a['units']:
                        new_string = '$\it{(}'
                        for string in val_a['units'].split():
                            new_string += "\it{" + string + "}\ "
                        new_string = new_string[:-2]
                        new_string += '\it{)}$'
                        labels[val_a['col_name']] = val_a['name'].title() + " " + new_string

                    # otherwise, itilicize the whole string
                    else:
                        labels[val_a['col_name']] = val_a['name'].title() + " $\it{(" + val_a['units'] + ")}$"

                # otherwise, use the column name
                else:
                    labels[val_a['col_name']] = val_a['col_name']

        # return the dictionary of now nicely formatted labels
        return labels

    def generate_label(self, measured_params, varied_params):

        label = {}

        try:

            for key, value in measured_params.items():
                label_key = value['mean_col_name']
                label_value_name = value['name']
                label_value_units = value['units']
                label_value = f"{label_value_name} ({label_value_units})"
                label[label_key] = label_value

            for key, value in varied_params.items():
                label_key = value['col_name']
                label_value_name = value['name']
                label_value_units = value['units']
                label_value = f"{label_value_name} ({label_value_units})"
                label[label_key] = label_value

        except Exception as e:
            self.logger.error(e)

        return label

    """
    Below are the plot methods
    """

    def create_title_page(self):

        page_len = 1 + 0.25 * (len(self.varied_params) + len(self.measured_params))

        spacing = page_len - 0.25

        title_figure: plt.Figure = plt.figure(figsize=(8.5, page_len))
        title_figure.text(0.5 / 8.5, spacing / page_len, f"Campaign: {self.campaign_name}", ha='left',
                          va='center',
                          fontsize=10)
        spacing = spacing - 0.25

        i = 1
        for key, param in self.varied_params.items():
            title_figure.text(0.5 / 8.5, spacing / page_len,
                              f"Varied param {i}: {param['name']}, {param['units']}",
                              ha='left', va='center',
                              fontsize=10)
            spacing = spacing - 0.25
            i = i + 1

        l = 1
        for key, param in self.measured_params.items():
            title_figure.text(0.5 / 8.5, spacing / page_len,
                              f"Measured param {l}: {param['name']}, {param['units']}",
                              ha='left',
                              va='center',
                              fontsize=10)
            spacing = spacing - 0.25
            l = l + 1

        self.save_figure(title=f"Cover Page: {self.campaign_name}", figure=title_figure)

    def progress_plot_sample(self, sample_position_df, measured_params, varied_params):
        """
        creates a progress plot for each sample level measured parameter
        :param sample_position_df:
        :param measured_col_names:
        :param varied_col_names:
        :return:
        """

        # get the column names for the varied and measured parameters
        varied_col_names = [val['col_name'] for key, val in varied_params.items()]
        measured_col_names = [val['mean_col_name'] for key, val in measured_params.items()]

        # get a list of all the column names
        col_names = varied_col_names + measured_col_names

        # get nicely formatted labels for the column names
        labels = self.get_measured_and_varied_param_labels(measured_params, varied_params)
        labels['sample'] = 'Sample number'

        plot_title = f"Progress Plot: {self.campaign_name}"
        self.logger.info(f"Plotting: {plot_title}")
        fig_size = (11,
                    1.5 * (len(self.varied_params) + len(self.measured_params)))

        # create the progress plot figure using the passed dataframe, column names, labels, and plot title
        try:
            fig = mpl_pltr.create_parameter_progress_plot(sample_position_df,
                                                          col_names,
                                                          'sample',
                                                          labels=labels,
                                                          title=plot_title,
                                                          figsize=fig_size)

            self.save_figure(title=plot_title, figure=fig)

        except Exception as e:
            self.logger.error(e)

        # create the plotly version of the plot
        try:
            plotly_labels = self.generate_label(measured_params, varied_params)
            plotly_labels['sample'] = 'Sample number'
            fig = plotly_pltr.progress_plot(sample_position_df,
                                            col_names,
                                            'sample',
                                            labels=plotly_labels,
                                            title=plot_title)

            fig_json = fig.to_plotly_json()

            json_path = os.path.join(self.plotly_directory, f"{slugify(f'Plotly {plot_title}')}.json")
            html_path = os.path.join(self.plotly_directory, f"{slugify(f'Plotly {plot_title}')}.html")

            # save to json
            with open(json_path, 'w', encoding='UTF-8') as fp:
                json.dump(fig_json, fp, cls=plotly.utils.PlotlyJSONEncoder)

            # save to html
            plotly.offline.plot(fig, filename=html_path, auto_open=False)

        except Exception as e:
            self.logger.error(e)


    def sample_dispense_accuracy_plot(self, sample_dispense_df):
        """
        creates a dispense plot for all dispense data
        :param sample_dispense_df:
        :return:
        """
        if sample_dispense_df is None:
            self.logger.warning(f"No sample_dispense data to plot.")
            return

        plot_title = f"Per Sample Dispense Accuracy: {self.campaign_name}"
        self.logger.info(f"Plotting: {plot_title}")

        # create the plotly version of the plot
        try:
            fig = plotly_pltr.sample_dispense_accuracy_plot(sample_dispense_df,
                                                            reference_column='optimiser_requested',
                                                            x_axis_column='sample',
                                                            y_axis_column='dispensed',
                                                            colour_column='attempt',
                                                            group_col='chemical')

            # update layout for web
            fig.update_layout(
                height=700 * len(sample_dispense_df['chemical'].unique()),
            )

            fig_json = fig.to_plotly_json()

            json_path = os.path.join(self.plotly_directory, f"{slugify(f'Dispensing: {plot_title}')}.json")
            html_path = os.path.join(self.plotly_directory, f"{slugify(f'Dispensing: {plot_title}')}.html")

            # save to json
            with open(json_path, 'w', encoding='UTF-8') as fp:
                json.dump(fig_json, fp, cls=plotly.utils.PlotlyJSONEncoder)

            # save to html
            plotly.offline.plot(fig, filename=html_path, auto_open=False)

        except:
            self.logger.exception("Failed to create dispense accuracy plot")

    def dispense_accuracy_plot(self, dispense_df):
        """
        creates a dispense plot for all dispense data
        :param sample_position_df:
        :param measured_col_names:
        :param varied_col_names:
        :return:
        """
        if dispense_df is None:
            self.logger.warning(f"No dispense_accuracy data to plot.")
            return

        plot_title = f"Overall Dispense Accuracy: {self.campaign_name}"
        self.logger.info(f"Plotting: {plot_title}")

        # create the plotly version of the plot
        try:
            fig = plotly_pltr.dispense_accuracy_plot(dispense_df,
                                   key_column='chemical',
                                   x_axis_column='request_volume',
                                   y_axis_column='dispense_volume')

            # update layout for web
            fig.update_layout(
                height=900,
            )

            fig_json = fig.to_plotly_json()

            json_path = os.path.join(self.plotly_directory, f"{slugify(f'Dispensing: {plot_title}')}.json")
            html_path = os.path.join(self.plotly_directory, f"{slugify(f'Dispensing: {plot_title}')}.html")

            # save to json
            with open(json_path, 'w', encoding='UTF-8') as fp:
                json.dump(fig_json, fp, cls=plotly.utils.PlotlyJSONEncoder)

            # save to html
            plotly.offline.plot(fig, filename=html_path, auto_open=False)

        except:
            self.logger.exception("Failed to create dispense accuracy plot")

    def corner_plot(self, sample_position_df, measured_params, varied_params):

        varied_col_names = [val['col_name'] for key, val in varied_params.items()]

        for key, val in measured_params.items():

            try:
                key = str(key)
                plot_name = (' ').join(key.split('_'))
                plot_name = plot_name.capitalize()

            except:
                plot_name = key

            plot_title = f"Corner Plot: {self.campaign_name}, {plot_name}"
            self.logger.info(f"Plotting: {plot_title}")
            labels = self.get_measured_and_varied_param_labels(measured_params, varied_params)

            fig_size = (1.5 * (len(self.varied_params) + len(self.measured_params)),
                        1.5 * (len(self.varied_params) + len(self.measured_params)))

            fig = mpl_pltr.create_corner_plot(sample_position_df,
                                              varied_col_names,
                                              val['mean_col_name'],
                                              title=plot_title,
                                              labels=labels,
                                              figsize=fig_size)

            self.save_figure(title=plot_title, figure=fig)

    def parallel_coordinates_plot(self, sample_position_df, measured_params, varied_params):

        varied_col_names = [val['col_name'] for key, val in varied_params.items()]

        for key, val in measured_params.items():

            try:
                key = str(key)
                plot_name = (' ').join(key.split('_'))
                plot_name = plot_name.capitalize()

            except:
                plot_name = key

            plot_title = f"Parallel Coordinates: {self.campaign_name}, {plot_name}"
            self.logger.info(f"Plotting: {plot_title}")

            labels = self.get_measured_and_varied_param_labels(measured_params, varied_params)

            fig_size = (1.5 * (len(self.varied_params) + len(self.measured_params)), 8.5)

            fig = mpl_pltr.create_parallel_coordinate_plot(sample_position_df,
                                                           varied_col_names,
                                                           val['mean_col_name'],
                                                           title=plot_title,
                                                           labels=labels,
                                                           figsize=fig_size)

            self.save_figure(title=plot_title, figure=fig)

    def spectra_plots(self, spectra_df, sample_df, varied_params, spectra_config=spectra_config):

        # Plots spectroscopy plots accross various samples and positions
        spec_df = pd.merge(spectra_df, sample_df, on=['sample', 'campaign'], how='outer')
        spec_df = spec_df[spec_df['x_(nm)'] > 275]

        # Plot the spectroscopy with respect to the varied parameters
        for key, val in varied_params.items():
            for name, plot in spectra_config.items():
                # Set the plot table
                plot_title = f"Spectroscopy Plot: {self.campaign_name}, Graph: {(' '.join(name.split('_'))).capitalize()}, {val['col_name']}"
                self.logger.info(f"Plotting: {plot_title}")
                # Get the plot details from the spectra config file. Update with information specific to this plot.
                plot['title'] = plot_title
                plot['data'] = spec_df
                plot['varied_col_name'] = val['col_name']
                plot['labels'] = self.get_measured_and_varied_param_labels(varied_params=varied_params)
                fig = mpl_pltr.generate_spectra_plot(**plot)

                self.save_figure(title=plot_title, figure=fig)

    def conductance_plots(self, conductance_df, sample_df, varied_params, measured_params, save_ind_pos=True):

        cond_df = pd.merge(conductance_df, sample_df, on=['sample', 'campaign', 'position'])

        outlier_df = cond.flag_outliers(data=conductance_df)

        for sample in outlier_df['sample'].unique():
            subset_df = outlier_df[outlier_df['sample'] == sample]
            sample_aggregate = []

            for position in outlier_df['position'].unique():
                position_df = subset_df[subset_df['position'] == position]
                clean_df = cond.remove_saturated_points(data=position_df)

                number_points = len(position_df)
                valid_points = len(clean_df)
                valid_fraction = valid_points / number_points
                r2, slope, intercept = cond.linear_regression_RANSAC(data=clean_df)

                df = pd.DataFrame(data={'position': [position],
                                        'r2': [r2],
                                        'slope': [slope],
                                        'valid_fraction': [valid_fraction],
                                        'valid_points': [valid_points],
                                        'total_points': [number_points]
                                        })
                try:
                    if save_ind_pos:
                        title = f"Conductivity Plot: {self.campaign_name}, Sample: {sample}, Position: {position}, Graph: Current vs. Voltage"
                        self.logger.info(f"Plotting: {title}")
                        cond_plt = mpl_pltr.conductance_plot(conductivity_data=position_df,
                                                             varied_param={
                                                                 'position': {'col_name': 'position', 'units': '#'}},
                                                             title=title,
                                                             individual_pos=True)

                        self.save_figure(title=title, figure=cond_plt)
                except Exception as e:
                    self.logger.warning(e)
                    self.logger.warning('Could not generate conductance plots by sample and position')

                sample_aggregate.append(df)

            sample_aggregate = pd.concat(sample_aggregate)

            # Conductance sample scatter
            title = f"Conductivity Plot: {self.campaign_name}, Sample: {sample}, Graph: Position vs. Conductance"
            self.logger.info(f"Plotting: {title}")
            cond_scatter = mpl_pltr.scatter_plot(data=sample_aggregate,
                                                 title=title,
                                                 x_axis_col="position",
                                                 y_axis_col="slope",
                                                 color_by_col="valid_fraction",
                                                 x_axis_label="Position",
                                                 y_axis_label="Conductance",
                                                 sample=sample)

            self.save_figure(title=title, figure=cond_scatter)

            if all(slope == 0 for slope in sample_aggregate['slope']):
                pass

            else:
                # Conductance Normal Distribution
                title = f"Conductivity Plot: {self.campaign_name}, Sample: {sample}, Graph: Normal Distribution Conductance"
                self.logger.info(f"Plotting: {title}")
                cond_normal = mpl_pltr.normal_distribution_plot(data=sample_aggregate,
                                                                title=title,
                                                                col="slope",
                                                                col_label="Conductance",
                                                                sample=sample)

                self.save_figure(title=title, figure=cond_normal)

            # r2 sample scatter
            title = f"Conductivity Plot: {self.campaign_name}, Sample: {sample}, Graph: Position vs. R-Squared"
            self.logger.info(f"Plotting: {title}")
            r2_scatter = mpl_pltr.scatter_plot(data=sample_aggregate,
                                               title=title,
                                               x_axis_col="position",
                                               y_axis_col="r2",
                                               color_by_col="valid_fraction",
                                               x_axis_label="Position",
                                               y_axis_label="R Squared",
                                               sample=sample)

            self.save_figure(title=title, figure=r2_scatter)

            # r2 beta distribution
            title = f"Conductivity Plot: {self.campaign_name}, Sample: {sample}, Graph: R-Squared Distribution"
            self.logger.info(f"Plotting: {title}")
            r2_beta = mpl_pltr.beta_distribution_plot(data=sample_aggregate,
                                                      title=title,
                                                      col="r2",
                                                      col_label="R Squared",
                                                      sample=sample)

            self.save_figure(title=title, figure=r2_beta)

        outlier_df = cond.flag_outliers(data=conductance_df)

        for sample in outlier_df['sample'].unique():
            subset_df = outlier_df[outlier_df['sample'] == sample]
            sample_aggregate = []

            for position in outlier_df['position'].unique():
                position_df = subset_df[subset_df['position'] == position]
                clean_df = cond.remove_saturated_points(data=position_df)

                number_points = len(position_df)
                valid_points = len(clean_df)
                valid_fraction = valid_points / number_points
                r2, slope, intercept = cond.linear_regression_RANSAC(data=clean_df)

                df = pd.DataFrame(data={'position': [position],
                                        'r2': [r2],
                                        'slope': [slope],
                                        'valid_fraction': [valid_fraction],
                                        'valid_points': [valid_points],
                                        'total_points': [number_points]
                                        })

                sample_aggregate.append(df)

            sample_aggregate = pd.concat(sample_aggregate)

            try:
                # Conductance sample scatter
                title = f"Conductivity Plot: {self.campaign_name}, Sample: {sample}, Graph: Position vs. Conductance Outliers"
                self.logger.info(f"Plotting: {title}")
                cond_scatter = mpl_pltr.scatter_plot(data=sample_aggregate,
                                                     title=title,
                                                     x_axis_col="position",
                                                     y_axis_col="slope",
                                                     color_by_col="valid_fraction",
                                                     x_axis_label="Position",
                                                     y_axis_label="Conductance",
                                                     sample=sample)

                self.save_figure(title=title, figure=cond_scatter)


            except Exception as e:
                self.logger.warning(e)

            try:
                if all(slope == 0 for slope in sample_aggregate['slope']):
                    pass

                else:
                    # Conductance Normal Distribution
                    title = f"Conductivity Plot: {self.campaign_name}, Sample: {sample}, Graph: Normal Distribution"
                    self.logger.info(f"Plotting: {title}")
                    cond_normal = mpl_pltr.normal_distribution_plot(data=sample_aggregate,
                                                                    title=title,
                                                                    col="slope",
                                                                    col_label="Conductance",
                                                                    sample=sample)

                    self.save_figure(title=title, figure=cond_normal)


            except Exception as e:
                self.logger.warning(e)

            try:
                # r2 sample scatter
                title = f"Conductivity Plot: {self.campaign_name}, Sample: {sample}, Graph: R-Squared Scatter"
                self.logger.info(f"Plotting: {title}")
                r2_scatter = mpl_pltr.scatter_plot(data=sample_aggregate,
                                                   title=title,
                                                   x_axis_col="position",
                                                   y_axis_col="r2",
                                                   color_by_col="valid_fraction",
                                                   x_axis_label="Position",
                                                   y_axis_label="R Squared",
                                                   sample=sample)

                self.save_figure(title=title, figure=r2_scatter)


            except Exception as e:
                self.logger.warning(e)

            try:
                # r2 beta distribution
                title = f"Conductivity Plot: {self.campaign_name}, Sample: {sample}, Graph: R-Squared Distribution"
                self.logger.info(f"Plotting: {title}")
                r2_beta = mpl_pltr.beta_distribution_plot(data=sample_aggregate,
                                                          title=title,
                                                          col="r2",
                                                          col_label="R Squared",
                                                          sample=sample)

                self.save_figure(title=title, figure=r2_beta)

            except Exception as e:
                self.logger.warning(e)

        plot_df = cond.remove_saturated_points(data=cond_df)

        mean_col_name = ''
        std_col_name = ''
        for param, val in measured_params.items():
            if 'conductivity' in param:
                mean_col_name = val['mean_col_name']
                std_col_name = val['std_col_name']

        for key, val in varied_params.items():
            color = val['col_name']
            param = {key: val}
            labels = self.get_measured_and_varied_param_labels(varied_params=varied_params)

            title = f"Conductivity Plot: {self.campaign_name}, Graph: Conductance ratio by Positon, {color}"
            self.logger.info(f"Plotting: {title}")
            ratio_pos_plot = mpl_pltr.create_conductance_ratio_position_plot(data=plot_df,
                                                                             slope='_conductivity_after_annealing_dataframe_slope',
                                                                             varied_params=varied_params,
                                                                             labels=labels,
                                                                             title=title,
                                                                             varied_parameter_column_name=color)
            self.save_figure(title=title, figure=ratio_pos_plot)

            title = f"Conductivity Plot: {self.campaign_name}, Graph: Conductivity vs. Voltage, {color}"
            self.logger.info(f"Plotting: {title}")
            cond_volt_plot = mpl_pltr.conductance_plot(conductivity_data=cond_df,
                                                       varied_param=param,
                                                       title=title)

            self.save_figure(title=title, figure=cond_volt_plot)

            title = f"Conductivity Plot: {self.campaign_name}, Graph: Conductance ratio by Sample, {color}"
            self.logger.info(f"Plotting: {title}")
            ratio_sample_plot = mpl_pltr.create_conductance_ratio_sample_plot(data=plot_df,
                                                                              mean_conductance_col_name=mean_col_name,
                                                                              std_conductance_col_name=std_col_name,
                                                                              varied_params=varied_params,
                                                                              labels=labels,
                                                                              varied_parameter_column_name=color,
                                                                              title=title,
                                                                              standard_error='_conductivity_after_annealing_dataframe_std_err')

            self.save_figure(title=title, figure=ratio_sample_plot)

    def plot_images(self):

        for key, value in self.images.items():
            plot_title = f"Image: {self.campaign_name}, Sample: {int(value['sample'].replace('sample_', ''))}, Number: {key}"

            self.logger.info(f"Plotting: {plot_title}")

            fig = mpl_pltr.plot_image(image_path=value['path'],
                                      title=plot_title,
                                      crack=f"{value['crack'] * 100}%",
                                      dewetting=f"{value['dewetting'] * 100}%",
                                      blurriness=f"{value['blur'] * 100}%",
                                      brightness=f"{value['brightness'] * 100}%"
                                      )

            self.save_figure(title=plot_title, figure=fig)

    """
    Plot methods end here
    """

    def generate_plots(self):

        self.create_title_page()

        # fill the sample df with mol ratio columns
        if self.sample_df is not None:
            if 'spiro_mols' in self.sample_df.keys():
                self.sample_df = self.populate_mol_ratio_columns(self.sample_df, 'spiro_mols')

        # merge sample and position df
        if self.sample_df is not None and self.position_df is not None:
            try:
                self.logger.info("Creating sample_position dataframe")
                self.sample_position_df = self.get_sample_and_positional_dataframe()
            except Exception as e:
                self.logger.warning(e)
                self.logger.warning("Could not get sample_position dataframe")

            # generate plots
            try:
                self.logger.info("Generating Progress Plots")
                self.progress_plot_sample(self.sample_position_df, self.measured_params,
                                          self.varied_params)
            except Exception as e:
                self.logger.warning(e)
                self.logger.warning("Could not generate progress plot")

            # dispense plots
            try:
                self.logger.info("Generating Overall Dispense Plots")
                self.dispense_accuracy_plot(self.dispense_df)
            except:
                self.logger.exception("Could not generate Overall Dispense plot")

            try:
                self.logger.info("Generating Sample Dispense Plots")
                self.sample_dispense_accuracy_plot(self.sample_dispense_df)
            except:
                self.logger.exception("Could not generate Sample Dispense plot")

            try:
                self.logger.info("Generating Corner Plots")
                self.corner_plot(self.sample_position_df, self.measured_params, self.varied_params)
            except Exception as e:
                self.logger.warning(e)
                self.logger.warning("Could not generate corner plot")

            try:
                self.logger.info("Generating Parallel Coordinates Plots")
                self.parallel_coordinates_plot(self.sample_position_df, self.measured_params,
                                               self.varied_params)
            except Exception as e:
                self.logger.warning(e)
                self.logger.warning("Could not generate parallel coordinates plot")

        # generate raw plots if that data exists
        if self.spectra_df is not None:

            try:
                self.logger.info("Generating Spectra Plots")
                self.spectra_plots(self.spectra_df, self.sample_df, self.varied_params)
            except Exception as e:
                self.logger.warning(e)
                self.logger.warning("Could not generate spectra plots")

        if self.conductance_df is not None:
            try:
                self.logger.info("Generating Conductance Plots")
                self.conductance_plots(self.conductance_df, self.sample_position_df, self.varied_params,
                                       self.measured_params)
            except Exception as e:
                self.logger.warning(e)
                self.logger.warning("Could not generate conductance plots")

        if len(self.dp.global_state.images) > 0:
            try:
                self.logger.info("Generating Image Plots")
                self.plot_images()

            except Exception as e:
                self.logger.warning(e)
                self.logger.warning("Could not plot images")

    def save_figure(self, title, figure):

        try:
            # Save the SVG
            plot_name = f"{slugify(title)}.svg"
            file_directory = os.path.join(self.matplotlib_plot_directory, plot_name)
            plt.savefig(file_directory, format="svg")

            fig_size = figure.get_size_inches()
            figure.text((fig_size[0] - 0.5) / fig_size[0], 0.5 / fig_size[1], f"{self.pdf_page_count}",
                        ha='center', fontsize=10)

            self.pdf_page_count = self.pdf_page_count + 1
            self.pdf.savefig(figure)

            plt.close('all')
            # force garbage collection
            gc.collect()

        except Exception as e:
            self.logger.warning(f"Failed to save plot: {plot_name}")
            self.logger.warning(e)

    def main(self):

        self.customization()
        self.generate_plots()

        self.pdf.close()
        plt.close('all')

    def customization(self):
        """
        this method will be called after the data has been processed but before the plots are generated. use this space
        to add any customizations to the plots. by default this function will do nothing.
        to edit the varied parameters: see self.varied_params
        to edit the measured parameters: see self.measured_params
        to edit the data: see self.spectra_df, self.conductance_df, self.position_df, self.sample_df
        :return: none
        """
        pass


if __name__ == '__main__':
    paths_to_campaign = ['/Users/teddyhaley/PycharmProjects/ada_processing/test_data/2019-08-22_10-47-11']
    dp = DataPlotter(paths_to_campaigns=paths_to_campaign)
    dp.main()
