from __future__ import print_function

# import nodes
from ada_processing.nodes.aggregation_nodes import AggregateRawConductivity, AggregateRawSpectra, \
    AggregateSampleConductivity, \
    AggregateSampleGaussROI, AggregateSamplePseudomobility, MergeAndAggSampleData, \
    MergeAndAggPositionData, AggregateDispenseData
from ada_processing.nodes.data_manipulation_nodes import ProcessImage, CalculateMols, CalculateAbsorbance, \
    CalculateFilmAbsorbance, CalculatePseudomobility, GaussianAreaUnderCurve, GaussianLimfitMinimize, LinearRegression, DispenseDataToDF, SampleDispenseDataToDF
from ada_processing.nodes.file_manipulation_nodes import LoadCSV, LoadDill, LoadJson, FolderObjLabeller, PushDictItems, \
    PushSubdirectories, LoadImage, PushJSONFiles
from ada_processing.nodes.misc_nodes import NoMath, Pusher, SpectraLabeller, CreateVariedParamObjects, Aligner
from ada_processing.nodes.data_validation_nodes import ValidateImage, ValidateConductivity

# import other ada_processing
from ada_processing.data_class import Data
from ada_processing.data_processor_helpers import setup_logging, suppress_stdout_stderr, log_file_sample_check

import logging
import os
import json
from consecution import Pipeline, GlobalState
import numpy as np

logger = logging.getLogger(__name__)

os.environ["PATH"] += os.pathsep + 'C:/Program Files (x86)/Graphviz2.38/bin/'


# items with their name in this list will be treated like Chemicals - they will be converted to mols, and their mol
# ratio calculated later.

def varied_params_router(item: Data):
    if item.metadata['path'] == 'chemical':
        return 'calculate_mols'
    else:
        return 'no_math'


class DataProcessor:
    """
    :param model_name: name of the image classification model to use
    """

    def __init__(self,
                 generate_plot_png=False):

        setup_logging()
        self.raw_cond_df = None
        self.raw_spec_df = None
        self.processed_positional_df = None
        self.processed_sample_df = None
        self.global_state = None

        # create the pipe
        self.validation_subpipe = None
        self.pipe = self.pipe_factory()

        # attempt to plot the pipe, continue if it fails
        if generate_plot_png:
            try:
                with suppress_stdout_stderr():
                    self.pipe.plot(file_name="loader_pipe")
            except Exception as e:
                logger.warning(e)
                logger.warning('Consecution graph not generated, check graphvis installation.')

    def pipe_factory(self):
        """
        This is where all the piping happens.
        NOTE: the | symbol sends the OUTPUT of the previous function to the INPUT of the next function, example:
        foo() | bar()
        the output of foo() will be the input of bar()
        this also works with lists of functions:
        foo() | [bar(), foo()] -> the output of foo() is the input of both bar() and foo() inside the list
        [bar(), foo()] | foo() -> the outputs of both bar() and foo() will be input into foo()
        :return: a pipe
        """

        self.validation_subpipe = Pipeline(

            Pusher('loader') |

            (PushSubdirectories('sample_directories', filter={'c-s-p': 's'}) |
             FolderObjLabeller('sample_labeller') |
             [
                 (PushSubdirectories('position_directories', filter={'c-s-p': 'p'}) |
                  FolderObjLabeller('position_labeller') |
                  [
                      ValidateConductivity('validate_conductivity', x_axis='Voltage', y_axis='Current',
                                           filter={'name': '_conductivity.csv', 'c-s-p': 'f'}),
                  ]),
                 ValidateImage('validate_image', filter={'filetype': 'jpg'}),
             ]),

        )

        # make the subpipe for varied parameters
        varied_fixed_params_subpipe = (
                PushDictItems('campaign_dispensed_amounts_subpipe', filter={'key_name':
                                                                                'campaign_dispensed_amounts'}) |
                CreateVariedParamObjects('create_varied_param_objects') |
                [
                    varied_params_router,
                    CalculateMols('calculate_mols'),
                    (NoMath('no_math'))
                ] |
                Aligner('agg_varied_params', startswith='sample')
        )

        # make the subpipe for the exp parameters json
        exp_params_subpipe = (
                LoadJson('sample_load_exp_params_json', filter={'name': '_experiment-parameters.json', 'c-s-p': 'f'}) |
                PushDictItems('sample_push_exp_params_dict') |
                varied_fixed_params_subpipe)

        # make the subpipe for conductivity csv files
        conductivity_subpipe = (

                LoadCSV('pos_validate_conductivity', x_axis='Voltage', y_axis='Current',
                        filter={'name': '_conductivity.csv', 'c-s-p': 'f'}) |
                LinearRegression('pos_conductivity_linregress', x_axis='Voltage', y_axis='Current') |
                AggregateSampleConductivity('sample_agg_conductivity')
        )

        spectra_subpipe = (
                Pusher('spectra_subpipe_entrance', filter={'c-s-p': 'f'}) |
                SpectraLabeller('spectra_labeller') |
                LoadDill('pos_spectra_load_dill', filter={'filetype': 'dill'}) |
                # TODO: Place Spectra Validation here
                [
                    [
                        CalculateAbsorbance('pos_spectra_absorbance', filter={'blank': False}),
                        CalculateAbsorbance('pos_spectra_absorbance_blank', filter={'blank': True})
                    ],
                    (CalculateFilmAbsorbance('pos_spectra_film_absorbance') |
                     GaussianLimfitMinimize('pos_spectra_gauss_fit') |
                     GaussianAreaUnderCurve('pos_spectra_gauss_area')),
                ]
        )

        image_analysis_subpipe = (
                LoadImage('load_image', filter={'c-s-p': 'f', 'filetype': 'jpg'}) |
                ProcessImage('process_image', filter={'c-s-p': 'f', 'filetype': 'jpg'})
        )

        # JSON file path

        json_to_csv_subpipe = (
                PushJSONFiles('json_filter') |
                LoadJson('json_loader') |
                DispenseDataToDF('dispense_accuracy_loader', df_name='dispense_accuracy')
        )

        # dispense per sample data subpipe
        sample_dispense_subpipe = (
            SampleDispenseDataToDF('sample_dispense_loader') |
            AggregateDispenseData('agg_sample_dispense', axis=0, df_name='sample_dispense_accuracy')
        )

        global_state = GlobalState(dataframes={},
                                   metadata={},
                                   varied_params={},
                                   measured_params={},
                                   images={},
                                   T_0=None,
                                   R_0=None)

        # make the overarching pipe that combines all the subpipes
        pipe = Pipeline(

            Pusher('loader') |

            PushSubdirectories('campaign_directories', filter={'c-s-p': 'c'}) |
            FolderObjLabeller('campaign_labeller') |

            [
                (PushSubdirectories('sample_directories', filter={'c-s-p': 's'}) |
                 FolderObjLabeller('sample_labeller') |
                 [
                     (PushSubdirectories('position_directories', filter={'c-s-p': 'p'}) |
                      FolderObjLabeller('position_labeller') |
                      [

                          conductivity_subpipe,
                          spectra_subpipe,
                      ]),

                     exp_params_subpipe,
                     image_analysis_subpipe,
                 ]),

            ],
            global_state=global_state
        )

        # Connect the downstream pipe to the loader node.
        # This allows us to skip the campaign and/or sample level direcotries
        pipe['loader'].add_downstream(pipe['sample_directories'])
        pipe['loader'].add_downstream(pipe['position_directories'])
        pipe['loader'].add_downstream(json_to_csv_subpipe)

        # add the sample dispense loading
        pipe['sample_load_exp_params_json'].add_downstream(sample_dispense_subpipe)

        # connect the raw varied parameters to the aggregate varied param node. this causes the raw varied parameter
        # data to be stored in the sample level dataframe
        pipe['create_varied_param_objects'].add_downstream(pipe['agg_varied_params'])

        # create the pseudomobility calculation node
        calc_pseudomobility = CalculatePseudomobility('pos_calc_pseudomobility')

        # add the pseudomobility calculation node downstream from the conductivity linear regression node and the
        # spectra gaussian area node. this lets us calculate pseudomobility from our conductance and gaussian ROI
        pipe['pos_conductivity_linregress'].add_downstream(calc_pseudomobility)
        pipe['pos_spectra_gauss_area'].add_downstream(calc_pseudomobility)

        # create the aggregate pseudomobility node, which calculates the mean and std of pseudomobility for each sample
        sample_agg_pseudomobility = AggregateSamplePseudomobility('sample_agg_pseudomobility')

        # add the aggregate pseudomobility node downstream from the pseudomobility calculation node
        calc_pseudomobility.add_downstream(sample_agg_pseudomobility)

        # create the aggregate gaussian ROI node, which calculates the mean and std of the gaussian area for each sample
        sample_agg_gauss = AggregateSampleGaussROI('sample_agg_gauss_roi')

        # add the aggregate gaussian ROI node downstream from the gaussian ROI node.
        pipe['pos_spectra_gauss_area'].add_downstream(sample_agg_gauss)

        # create the aggregate raw spec node, which puts all (the whole campaign!) the raw spectra,
        # absorbance, and film absorbance data into one dataframe
        agg_sample_raw_spec = AggregateRawSpectra('agg_all_raw_spec', axis=0, transpose=False,
                                                  df_name='raw_spec')

        # add the aggregate raw spec node downstream from the raw spectra, absorbance, and film absorbance nodes
        pipe['pos_spectra_load_dill'].add_downstream(agg_sample_raw_spec)
        pipe['pos_spectra_absorbance'].add_downstream(agg_sample_raw_spec)
        pipe['pos_spectra_absorbance_blank'].add_downstream(agg_sample_raw_spec)
        pipe['pos_spectra_film_absorbance'].add_downstream(agg_sample_raw_spec)

        # create the aggregate raw conductance node, which puts all (the whole campaign!)
        # the raw conductance data into one dataframe.
        agg_sample_raw_cond = AggregateRawConductivity('agg_all_raw_cond', axis=0, transpose=False,
                                                       df_name='raw_cond')

        # add the agg raw sample conductance node downstream from the raw conductance node
        pipe['pos_validate_conductivity'].add_downstream(agg_sample_raw_cond)

        # create the merge and aggregate positional data node, which 1. merges all the positional data (i.e.
        # conductance, gaussian ROI, pseudomobility...) into one dataframe, and then 2. aggregates all of
        # these over the whole campaign.
        agg_pos_data = MergeAndAggPositionData('merge_and_agg_position_data', axis=0, transpose=False,
                                               df_name='processed_positional')

        # add the merge and agg positional data node downstream from all the positional level data nodes
        pipe['pos_conductivity_linregress'].add_downstream(agg_pos_data)
        pipe['pos_spectra_gauss_area'].add_downstream(agg_pos_data)
        calc_pseudomobility.add_downstream(agg_pos_data)

        # create the merge and aggregate sample data node,  which 1. merges all the sample data (i.e varied parameters,
        # crack score, mean and std conductance, mean and std pseudomobility, mean and std gaussian ROI...)
        # into one dataframe, then 2. aggregate all of them over the whole campaign.
        agg_all_sample_data = MergeAndAggSampleData('merge_and_agg_sample_data')

        # add merge and agg sample data node downstream from all the the sample level data nodes
        pipe['process_image'].add_downstream(agg_all_sample_data)
        pipe['sample_agg_conductivity'].add_downstream(agg_all_sample_data)
        pipe['agg_varied_params'].add_downstream(agg_all_sample_data)
        sample_agg_pseudomobility.add_downstream(agg_all_sample_data)
        sample_agg_gauss.add_downstream(agg_all_sample_data)

        # return the pipe we've constructed
        return pipe

    def process_campaigns(self, paths: list, dataframe_cache_folder_path=None, check_validity=False):
        """
        processes a campaign, producing CSVs and dataframes containing the raw and processed data
        :param paths: a list of paths
        :param dataframe_cache_folder_path: path to folder to store CSVs
        :return: dictionary of dataframes created during processing
        """

        # iterate through the campaign folders and turn them into data objects
        data_list = []
        i = 0
        name = ''
        if not isinstance(paths, list):
            paths = [paths]
        for path in paths:
            name = os.path.basename(path)
            parent = None
            metadata = {
                'c-s-p': 'c',
                'folder_name': name,
            }
            if check_validity:
                incomplete_samples = log_file_sample_check(path)
                metadata['incomplete_samples'] = incomplete_samples
            data = Data(data=path, name=f"campaign_{i}", parent=parent, metadata=metadata)
            data_list.append(data)
            i += 1

        # process the campaign. Sends the data to the pipeline.
        self.pipe.consume(data_list)

        # get the dictionary of dataframes resulting from processing the campaign
        return_dict = self.pipe.global_state.dataframes

        # add call to new measured param method here
        #
        #
        #
        #

        # set various self.attributes to different dataframes for easy access via helper methods
        self.set_attributes()

        # save the csvs of dataframes and jsons of parameters
        self.save_files(dataframe_cache_folder_path=dataframe_cache_folder_path, return_dict=return_dict, name=name)

        # attempt to move the error log
        try:
            os.rename(os.path.join(os.getcwd(), 'errors.log'),
                      os.path.join(os.getcwd(), 'zb-results', name, 'errors.log'))
        except Exception:
            pass

        # return the dictionary of dataframes
        return return_dict

    def save_files(self, name, dataframe_cache_folder_path, return_dict):

        # check if the dataframe cache folder exists, if not, make it
        if dataframe_cache_folder_path is None:
            folder_name = 'zb-results'
            os.makedirs(folder_name, exist_ok=True)
            dataframe_cache_folder_path = os.path.join(os.getcwd(), folder_name)

        # make the processed_data folder
        dataframe_cache_folder_path = os.path.join(dataframe_cache_folder_path, name, 'processed_data')
        os.makedirs(dataframe_cache_folder_path, exist_ok=True)

        logger.info(f"saving results to: {dataframe_cache_folder_path}")

        # save all the dataframes as csvs in the dataframe cache folder
        for key, df in return_dict.items():
            df.to_csv(os.path.join(dataframe_cache_folder_path, f"{name}_{key}") + '.csv')

        if hasattr(self.global_state, 'varied_params'):
            with open(os.path.join(dataframe_cache_folder_path, f"{name}_varied_params.json"), 'w') as fp:
                json.dump(self.global_state.varied_params, fp)

        if hasattr(self.global_state, 'measured_params'):
            with open(os.path.join(dataframe_cache_folder_path, f"{name}_measured_params.json"), 'w') as fp:
                json.dump(self.global_state.measured_params, fp)

        if hasattr(self.global_state, 'images'):
            with open(os.path.join(dataframe_cache_folder_path, f"{name}_images.json"), 'w') as fp:
                json.dump(self.global_state.images, fp)

    def set_attributes(self):
        """
        set various self.attributes to different dataframes for easy access via helper methods
        :return: none
        """

        # get the pipe's global state
        self.global_state = self.pipe.global_state

        # if there is conducitivity data, set self.raw_cond_df to the dataframe
        try:
            self.raw_cond_df = self.pipe.global_state.dataframes['raw_cond']
        except:
            logger.info("No raw conductivity data")

        # if there is spectra data, set self.raw_spec_df to the dataframe
        try:
            self.raw_spec_df = self.pipe.global_state.dataframes['raw_spec']
        except:
            logger.info("No raw spectra data")

        # if there is positional data, set self.processed_positional_df to the dataframe
        try:
            self.processed_positional_df = self.pipe.global_state.dataframes['processed_positional']
        except:
            logger.info("No positional data")

        # if there is sample data, set self.processed_sample_df to the dataframe
        try:
            self.processed_sample_df = self.pipe.global_state.dataframes['processed_sample']
        except:
            logger.info("No sample data")

        # add dispense data
        try:
            self.dispense_df = self.pipe.global_state.dataframes['dispense_accuracy']
        except:
            logger.info("No dispense accuracy data")

        # add dispense data
        try:
            self.sample_dispense_df = self.pipe.global_state.dataframes['sample_dispense_accuracy']
        except:
            logger.info("No dispense accuracy data")

    def get_sample_dispense_dataframe(self):
        """
        :return: raw conductivity dataframe if exists, else None
        """
        if hasattr(self, "sample_dispense_df"):
            return self.sample_dispense_df

    def get_dispense_dataframe(self):
        """
        :return: raw conductivity dataframe if exists, else None
        """
        if hasattr(self, "dispense_df"):
            return self.dispense_df

    def get_raw_cond_dataframe(self):
        """
        :return: raw conductivity dataframe if exists, else None
        """
        if hasattr(self, "raw_cond_df"):
            return self.raw_cond_df

    def get_raw_spec_dataframe(self):
        """
        :return: raw spectra dataframe if exists, else None
        """
        if hasattr(self, "raw_spec_df"):
            return self.raw_spec_df

    def get_processed_position_dataframe(self):
        """
        :return: positional dataframe if exists, else None
        """
        if hasattr(self, "processed_positional_df"):
            return self.processed_positional_df

    def get_processed_sample_dataframe(self):
        """
        :return: sample dataframe if exists, else None
        """
        if hasattr(self, "processed_sample_df"):
            return self.processed_sample_df

    def get_varied_params(self):
        """
        :return: dictionary of varied parameters with associated data
        """
        if hasattr(self.global_state, "varied_params"):
            return self.global_state.varied_params

    def get_measured_params(self):
        """
        :return: dictionary of measured parameters with associated data
        """
        if hasattr(self.global_state, "measured_params"):
            return self.global_state.measured_params

    def get_images(self):
        """
        :return: dictionary of measured parameters with associated data
        """
        if hasattr(self.global_state, "images"):
            return self.global_state.images

    def process_sample(self, path_to_sample):
        """
        processes a sample folder
        :param path_to_sample: path to the sample folder
        :return: none
        """
        # get the pipe created in the __init__ method
        pipe = self.pipe

        # wrap the path to the folder in a data object, specify that is's a sample with the 'c-s-p: s' metadata
        # we specify it's a sample so that the data pipeline knows that this is a sample folder not a campaign folder
        data = Data(data=path_to_sample, name=f"sample_000", parent=None,
                    metadata={'c-s-p': 's', 'folder_name': os.path.basename(path_to_sample)})

        # process the sample by having the pipe consume it
        pipe.consume([data])

        # extract the data from the pipe and store it in self for later use
        self.global_state = self.pipe.global_state

        # pick apart the global state into individual attributes
        self.set_attributes()

    def process_subpipe(self, pipe, sample_path):

        data = Data(data=sample_path, name=f"sample_000", parent=None,
                    metadata={'c-s-p': 's', 'folder_name': os.path.basename(sample_path)})

        # process the sample by having the pipe consume it
        pipe.consume([data])

    def get_specified_measured_param(self, param_name):
        """
        search through self.measured_params and return the specified measured param
        this is so you don't have to know the full param name to get it, only part of it.
        :param param_name: the param you wish to get
        :return: the param which contains the specified param name
        """
        for parameter, val in self.get_measured_params().items():
            if param_name in parameter:
                return val

    def get_sample_conductance(self, path_to_sample):
        """
        gets the conductance data for a given sample
        :param path_to_sample: path to sample folder
        :return: sample conductance data: slope for each position, mean, std
        """

        # process the sample
        self.process_sample(path_to_sample)

        # get the positional level dataframe
        df = self.get_processed_position_dataframe()

        # get the conductivity measured parameter
        parameter = self.get_specified_measured_param('conductivity')

        # get the column name of the column containing the slopes of the IV curves
        conductance_col = None
        for key in df.keys():
            if 'slope' in key:
                conductance_col = key
        if conductance_col is None:
            raise (Exception('No conductance column in positional data.'))

        # downselect the dataframe to the slope column
        df = df[conductance_col]

        # turn the dataframe into a dictionary to return
        dictionary = df.to_dict()

        # get the column names for the mean and std conductances
        conductance_mean_col = parameter['mean_col_name']
        conductance_std_col = parameter['std_col_name']

        # get the sample level dataframe
        df2 = self.get_processed_sample_dataframe()

        # convert the mean to a float
        dictionary['mean'] = float(df2[conductance_mean_col][0])

        # try to find the relative standard deviation, if std is 0 will fail, so return 0 if that is the case
        try:
            dictionary['std_rel'] = float(df2[conductance_std_col][0]) / dictionary['mean']
        except ZeroDivisionError:
            dictionary['std_rel'] = 0

        # return the slopes alongside the mean and standard deviation of conductance for the sample
        return dictionary

    def get_sample_pseudomobility(self, path_to_sample):
        """
        get the positional, mean, and standard deviation pseudomobility of a sample
        :param path_to_sample: path to the sample folder
        :return: dictionary containing the positional, mean, and std values of pseudomobility for the sample
        """

        # process the sample
        self.process_sample(path_to_sample)

        # get the positional level dataframe
        df = self.get_processed_position_dataframe()

        # get the pseudomobility measured parameter
        parameter = self.get_specified_measured_param('pseudomobility')

        # get the column name which contains the positional pseudomobility values
        pseudomobility_col = None
        for key in df.keys():
            if 'pseudomobility' in key:
                pseudomobility_col = key
        if pseudomobility_col is None:
            raise (Exception('No pseudomobility column in positional data.'))

        # downselect the dataframe to the pseudomobility column
        df = df[pseudomobility_col]

        # turn the pseudomobility column into a dictionary to return
        dictionary = df.to_dict()

        # get the column names for the mean and std pseudomobility for the sample
        pseudomobility_mean_col = parameter['mean_col_name']
        pseudomobility_std_col = parameter['std_col_name']

        # get the sample level dataframe
        df2 = self.get_processed_sample_dataframe()

        # convert the mean pseudomobility to a float and store it in the return dictionary
        dictionary['mean'] = float(df2[pseudomobility_mean_col][0])

        # attempt to get a relative standard deviation. if fails, set it to 0 instead.
        try:
            dictionary['std_rel'] = float(df2[pseudomobility_std_col][0]) / dictionary['mean']
        except ZeroDivisionError:
            dictionary['std_rel'] = 0

        # return a dictionary of the positional, mean, and std pseudomobilities of a sample
        return dictionary

    def get_sample_image_score(self, path_to_sample):
        """
        get the positional, mean, and standard deviation imaging score of a sample
        :param path_to_sample: path to the sample folder
        :return: dictionary containing the positional, mean, and std values of imaging score for the sample
        """

        # process the sample
        self.process_sample(path_to_sample)

        # get the positional level dataframe
        df = self.get_processed_sample_dataframe()

        dewetting_score = None
        crack_score = None

        for key in df.keys():

            if 'dewetting' in key and 'mean' in key:
                dewetting_score = df[key][0]

            if 'crack' in key and 'mean' in key:
                crack_score = df[key][0]

        return {'dewetting_score': dewetting_score,
                'crack_score': crack_score}

    def validate_sample(self, path_to_sample):
        # process the sample
        self.process_subpipe(pipe=self.validation_subpipe, sample_path=path_to_sample)

    # TODO: Temporary location
    def get_sample_cost(self, path_to_sample, anneal_time, cost_settings):
        def _exponential(
                value: float,
                max_value: float,
                exponent: float = 2,
        ):
            """
            Exponential decay function.
            :param value: The value to evaluate
            :param max_value: The maximum possible value
            :param exponent: The rate of decay
            :return: The result of the evaluation of the exponential decay function
            """
            return 1 - (value / max_value) ** exponent

        def logistic(
                x: float,
                midpoint: float = 0,
                maximum: float = 1,
                rate: float = 1,
        ):
            """
            Logistic funciton.  see: https://en.wikipedia.org/wiki/Logistic_function
            This function has been modified so that it decays, instead of grow.
            This function has been modified so that it intercepts at (0, 1) and (1, 0).
            :param x: The values to evaluate
            :param midpoint: The sigmoid's midpoint
            :param maximum: The curve's maximum value
            :param rate: The growth rate of the function
            :return: The result of the evaluation of the Logistic function
            """

            return ((1 + np.exp(rate * (- midpoint))) / \
                    (1 + np.exp(rate * (x - midpoint))))

        def cost(
                mobility: float,
                annealTime: float,
                crack: float,
                dewet: float,
                maxAnnealTime: float,
                crackPos: float,
                crackRate: float,
                dewetPos: float,
                dewetRate: float,

        ):
            """
            Arbitrary cost function for optimization
            :param mobility: Positive value for pseudomobility.
            :param annealTime: How long the film was annealed for in seconds.
            :param crack: Crackedness score, from 0 to 1, not cracked to cracked.
            :param dewet: Dewettedness score, from 0 to 1, to wetted, to dewetted.
            :param crackPos: Midpoint of crack cost
            :param crackRate: Rate of crack cost
            :param dewetPos: Midpoint of dewet cost
            :param dewetRate: Rate of dewet cost
            :return: Score, larger values are desired.
            """

            # The partial cost for each component is calculated.
            pseudoCost = mobility

            annealCost = 1.0#(1 - (annealTime / maxAnnealTime) ** 3)

            crackCost = 1
            # logistic(
            #     x=crack,
            #     midpoint=crackPos,
            #     maximum=1,
            #     rate=crackRate,
            # )

            dewetCost = 1
            # logistic(
            #     x=dewet,
            #     midpoint=dewetPos,
            #     maximum=1,
            #     rate=dewetRate
            # )
            # Calculate global cost as a product of each partial cost.
            cost = pseudoCost * annealCost * crackCost * dewetCost

            return cost

        mean_pseudomobility = self.get_sample_pseudomobility(path_to_sample)["mean"]
        image_score = self.get_sample_image_score(path_to_sample)
        crack_value = image_score["crack_score"]
        dewet_value = image_score["dewetting_score"]

        c = cost(mobility=mean_pseudomobility,
                    annealTime=anneal_time,
                    crack=crack_value,
                    dewet=dewet_value,
                    **cost_settings,
                    )

        logger.info(f"mean pseudomobility: {mean_pseudomobility}")
        logger.info(f"crack value: {crack_value}")
        logger.info(f"dewet value: {dewet_value}")
        logger.info(f"final cost score: {c}")

        return c


if __name__ == '__main__':
    dp = DataProcessor()
    path_to_sample_list = [r"/Users/teddyhaley/PycharmProjects/ada_processing/test_data/2019-09-19_13-42-52"]
    dp.process_campaigns(path_to_sample_list)