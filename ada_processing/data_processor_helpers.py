import logging
import glob
import pandas as pd
import re
import coloredlogs
import os
from collections import defaultdict
from ada_processing.data_class import Data
from contextlib import contextmanager, redirect_stderr, redirect_stdout


def setup_logging():
    """
    sets up the logging configuration
    """
    # set file logging settings
    file_handler = logging.FileHandler(filename="errors.log")
    file_handler.setLevel(logging.ERROR)
    logging.basicConfig(level=logging.DEBUG,
                        handlers=[file_handler])

    import absl.logging
    logging.root.removeHandler(absl.logging._absl_handler)

    # Set environment variables for coloured logging
    os.environ['COLOREDLOGS_LOG_FORMAT'] = "%(asctime)s: %(name)s: %(funcName)s(): %(levelname)s: %(message)s"
    os.environ['COLOREDLOGS_DATE_FORMAT'] = "%Y-%m-%d_%H-%M-%S-%f"
    os.environ['COLOREDLOGS_LEVEL_STYLES'] = "debug=green;info=white;warning=yellow;error=red;critical=red,bold"

    # The level of logger messages to be displayed to the console
    coloredlogs.install(level="INFO")

    # Diasble pandas warnings for chained assignment
    pd.options.mode.chained_assignment = None


def log_file_sample_check(campaign_folder, base_log_filename='campaign_log.log'):
    logger = logging.getLogger(__name__)

    path_str = os.path.join(campaign_folder, f"{base_log_filename}*")
    log_files = glob.glob(path_str)
    if len(log_files) == 0:
        logger.warning(f"provided log file path does not exist {path_str}, no samples will be skipped.")
        return []

    # read log file
    logs = []
    for l in log_files:
        with open(l, 'r') as fp:
            logs.extend(fp.readlines())

    # count all the started and finished samples in the
    patterns = {
        "started": re.compile(".*experiment iteration: ([0-9]+)"),
        "completed": re.compile(".*Iteration ([0-9]+) took.*"),
    }
    results = defaultdict(list)
    for line in logs:
        for k, p in patterns.items():
            m = p.search(line)
            if m is not None:
                logger.debug(f"Found: {m.string.strip()}, iteration number: {m.group(1)}")
                results[k].append(int(m.group(1)))

    results["incomplete"] = [x for x in results['started'] if x not in results["completed"]]
    logger.info(f"Completed sample information: {results}")

    return results['incomplete']


def aggregate_column(items: [Data], column):
    """
    takes a list of dataframes with a common column and returns the mean and std across those dataframes in that column.
    :param items: items to aggregate
    :param column: column name
    :return: mean and std across dataframes in the given column
    """
    df = pd.DataFrame(columns=[column])
    for item in items:
        df = df.append({column: item.data[column]}, ignore_index=True)

    aggregated = df[column]
    return aggregated.mean(), aggregated.std()


def aggregate_column_max(items: [Data], column):
    """
    takes a list of dataframes with a common column and returns the mean and std across those dataframes in that column.
    :param items: items to aggregate
    :param column: column name
    :return: mean and std across dataframes in the given column
    """
    df = pd.DataFrame(columns=[column])
    for item in items:
        df = df.append({column: item.data[column]}, ignore_index=True)

    aggregated = df[column]
    return aggregated.max()


def list_mean_std(item_list):
    """
    takes a list of values and returns the mean and std
    :param item_list: list to operate on
    :return: mean and std of list
    """
    series = pd.Series(item_list)
    return series.mean(), series.std()


def add_position_sample_campaign_columns(data, item: Data):
    """
    adds a position, sample, and campaign column to the given dataframe
    :param data: dataframe
    :param item: Data item with a parent
    :return: dataframe with position, sample, and campaign columns added to it
    """
    try:
        data['position'] = int(re.sub('[^0-9]', '', item.get_parent_name_startswith('pos')))
    except:
        pass
    try:
        data['sample'] = int(re.sub('[^0-9]', '', item.get_parent_name_startswith('sample')))
    except:
        pass
    try:
        data['campaign'] = int(re.sub('[^0-9]', '', item.get_parent_name_startswith('campaign')))
    except:
        pass
    return data


# for disabling underlying logging
@contextmanager
def suppress_stdout_stderr():
    """A context manager that redirects stdout and stderr to devnull"""
    with open(os.devnull, 'w') as fnull:
        with redirect_stderr(fnull) as err, redirect_stdout(fnull) as out:
            yield (err, out)
