# Ada Processing

Data processing and plotting for the ada project.

To process campaigns older than August 7, 2019, use version 2.0.0.

![Ada Processing Diagram](pics/ada_processing_diagram.png)

[Link to diagram](https://docs.google.com/presentation/d/1Vu38TPROavsnVzEdHnbiYvd-K-GtBptKzEhxDkQn57s/edit?usp=sharing)

Installation:
> pip>=19 is required to properly follow git urls within the ada_processing setup.py

For HTTPS gitlab username/password:
```bash
pip install --upgrade pip
pip install git+https://gitlab.com/ada-chem/ada_processing.git@master#egg=ada_processing
```
Or if you have SSH keys setup:
```bash
pip install --upgrade pip
pip install git+ssh://git@gitlab.com/ada-chem/ada_processing.git@master#egg=ada_processing
```

## Data Processor

For an introduction to consecution (the framework used for the pipeline), [click here](https://colab.research.google.com/drive/1UHgzh28Tw81eCXiq720d5qQ5xmb136UX)

See [the consecution github](https://github.com/robdmc/consecution) for a consecution tutorial from the dev, and his [readthedocs](https://consecution.readthedocs.io/en/latest/) for API documentation.

First, import the `DataProcessor` class:
```python
from ada_processing.data_processor import DataProcessor
```

Create a `DataProcessor` instance:
```python
from ada_processing.data_processor import DataProcessor

dp = DataProcessor()
```

To process a whole campaign, run `dp.process_campaign` with the path to campaign folder, or list of paths to campaign folders.
```python
from ada_processing.data_processor import DataProcessor

dp = DataProcessor()

path = ["/some/path"]
dp.process_campaigns(path)
```

To get information about a sample, run the appropriate `dp.get_sample` method with the path to a sample folder:
```python
from ada_processing.data_processor import DataProcessor

dp = DataProcessor()

path = ["/some/path"]
dp.get_sample_pseudomobility(path)
dp.get_sample_conductance(path)
```

## Data Plotter

The data plotter takes either a campaign folder or a processed zb-results campaign data folder and generates plots for the given campaign.

First, import the `DataPlotter` class:
```python
from ada_processing.data_plotter import DataPlotter
```

To process then plot a campaign, create a `DataPlotter` instance with the `paths_to_campaigns` argument, then run `dp.main()`:
```python
from ada_processing.data_plotter import DataPlotter

path_to_campaign_folder = ["/some/path"]
dp = DataPlotter(paths_to_campaigns=path_to_campaign_folder)
dp.main()
```

To plot a preprocessed campaign, create a `DataPlotter` instance with the `path_to_zb_results_campaign_folder` argument,run `dp.main()`

```python
from ada_processing.data_plotter import DataPlotter

path_to_zb_results_campaign_folder = "/some/path"
dp = DataPlotter(path_to_zb_results_campaign_folder=path_to_zb_results_campaign_folder)
dp.main()
```

## Development "How To"s

### Add a new varied parameter

To add a new varied parameter, ideally, nothing has to be done in ada_processing. To add the new varied parameter, add it to the experiment parameters json file in the zippleback repository.

### Add a new measured parameter

#### To add a BRAND new measured parameter (NOT a combination of other measured parameters) follow the steps below:

1. Identify whether the measured parameter is measured per sample or per position.

2. Create a node (or multiple nodes in a series) performing the desired operations on the data in the `data_manipulation_nodes.py` file.
    
    Follow these steps only if you wish to save the raw data:
    
    2a. If the raw data is desired to be kept, create a "Load_(type of data)" Node inside the `file_manipulation_nodes.py` file
    
    2b. Create a node that aggregates all the raw data over the whole campaign in the `aggregation_nodes.py` file. Follow the template of the other relevant nodes in that file.
    
    2c. Add the new dataframe to the `set_attributes` method inside the `DataProcessor` class
    
    2d. Add a new `get_` method to the `DataProcessor` class that returns this dataframe

3. Create a subpipe inside the `pipe_factory` method in the `data_processor.py` file with the node(s) relevant to this subpipe. look at the other subpipes for examples on how to do this.

4. If the measured parameter is per sample, connect the last node in the subpipe to the `merge_and_agg_sample_data` node. Skip to step 9.

5. If the measured parameter is per position, create another node in the `aggregation_nodes.py` file which aggregates that measurement over the positions and pushes the desired value(s) per sample onward (e.g. mean, std, max...)

6. Add the node created in 5. downstream from the last node in the subpipe created in 3.

7. Add the `merge_and_agg_position_data` node downstream from the last node in the subpipe created in 3.

8. Add the `merge_and_agg_sample_data` node downstream from the node created in 5.

9. Test that it works!

#### Add a new measured (contrived) parameter that is a combination of other, already existing measured parameters:

1. Create a method inside the `DataProcessor` class named `add_(param_name)`. This method will take the required dataframes as arguments, as well as the measured parameters dictionary

2. Inside the method created in 1, add a new column to the appropriate dataframe (either sample level dataframe or position level dataframe) with the correct value of the new parameter, e.g.: `df['new_param'] = df['old_param'] * df['old_param_2']`

3. Add the details of the new measured parameter to the measured parameters dictionary, e.g.:
    ```python
    measured_params = self.get_measured_params()
    measured_params['new_param'] = {}
    measured_params['new_param']['mean_col_name'] = ''
    measured_params['new_param']['std_col_name'] = ''
    measured_params['new_param']['units'] = ''
    measured_params['new_param']['name'] = ''
    ```
4. Call this method inside `DataProcessor.process_campaigns()`, right before the call to `set_attributes()`

### Add a new plot to the plotter

1. Create a method inside the data plotter class which uses the `self.(df_name)_df` attributes and the `self.measured_params` and `self.varied_params` attributes to create the desired plot. Have this function return a List of matplotlib figures. Look at the other plotting functions for examples.

2. Add the call to this function to `self.generate_plots()`. Look at that function for examples.

### Spectra Config

Inside the `configuration` folder, there is a file called `spectra_config.py`. This file contains nested python dictionaries which tell the data plotter the details of how to create the spectra plots, and their customization. If you wish to edit the spectra plots, or add new spectra plots, look through this file and edit appropriately.

### Expected Folder and File Structure

![Expected Folder File Structure](pics/expected_folder_file_structure.png)

- Campaign Folder
    - Sample Folder
        - Position Folder
            - Uvvis Files
            - Conductivity Files
        - More position folders...
        - Experiment Parameters Json File
        - Images
    - More sample folders...
        