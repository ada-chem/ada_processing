#!/usr/bin/env bash

# Add pre-downloaded data for testing in docker
# export TEST_DATA_PATH=./test_data

export LOCAL_ZB_DATA="../test_data"
export LOCAL_DASH_PLOTS=~/media/dash-plots

#docker rmi $(docker images -a -q)docker rmi $(docker images -a --filter=dangling=true -q)
#docker rm $(docker ps --filter=status=exited --filter=status=created -q)
#docker rmi $(docker images -a -q)

# add test path for testing
docker-compose up --build
