from ada_processing.data_plotter import DataPlotter
import logging
import os
import time

logging.getLogger().setLevel(logging.INFO)
logger = logging.getLogger(__name__)


if __name__ == "__main__":

    # Campaign last modified time
    last_mod_time = {}
    while True:
        logger.info(f"iterating through campaign folders from local...")
        # Sort the values alphabetically (by campaign name) so most recent is run first
        campaigns = sorted(os.scandir("/zb-data/"), key=lambda x: x.name, reverse=True)
        for campaign in campaigns:
            if not campaign.name.startswith('.') and campaign.is_dir() and 'archive' not in campaign.name:
                logger.debug(f"Found: {campaign.name}")
                # checks if the path still exists before walking
                if not os.path.exists(campaign.path):
                    logger.warning(f"Path no longer exists: {campaign.path}")
                    continue
                # checks to see the last modified time
                mod_time = max(map(lambda x: os.path.getmtime(x[0]), os.walk(campaign.path)))
                logger.debug(f"Last modified time for `{campaign.name}`` was at: {time.ctime(mod_time)}")
                # Check whether the directory has been modified since program was last started.
                if mod_time == last_mod_time.get(campaign.name, -1):
                    logger.debug(f"{campaign.name} unchanged, skipping...")
                    continue

                # Update the stored time
                last_mod_time[campaign.name] = mod_time

                # Check there is at least 1 subdirectory before running, if not skip it
                if not any([(not sample.name.startswith('.')) and (sample.is_dir()) for sample in os.scandir(campaign.path)]):
                    continue

                logger.info(f"Plotting from local path {campaign.path}")
                try:
                    # check validity uses the log file to filter campaigns
                    dp = DataPlotter(paths_to_campaigns=[campaign.path], check_validity=True, skip_specra=True)
                    dp.main()
                except Exception as e:
                    logger.exception(f"unable to plot campaign {campaign.name} "
                                     f"automatically, please debug locally.")

                # If we successfully updated, start again to check the newest campaigns
                break

            else:
                logger.warning(f"Expecting directories only in top level zb-data, ignoring: {campaign.name}")
                continue

        logger.info("Sleeping")
        time.sleep(10)
