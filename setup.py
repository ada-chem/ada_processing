from setuptools import setup, find_packages

NAME = 'ada_processing'
DESCRIPTION = "This package contains scripts to process data from ada projects as well as generate plots, csv's and reports"
AUTHOR = ''
URL = 'https://gitlab.com/ada-chem/ada_processing'
CLASSIFIERS = [
    "Programming Language :: Python :: 3",
    "License :: OSI Approved :: MIT License",
    "Operating System :: OS Independent",
]
INSTALL_REQUIRES = [
    "coloredlogs",
    'dill',
    "pandas",
    "numpy",
    "consecution",
    "ada_toolbox @ git+https://gitlab.com/ada-chem/toolbox.git@master#egg=ada_toolbox",
    "ada_imaging @ git+https://gitlab.com/ada-chem/ada_imaging.git@master#egg=ada_imaging",
    "plotly",
    "plotly-express",
    "matplotlib",
    "scipy",
    "img2pdf",
    "psutil",
    "gitpython",
]

about = {}
with open('ada_processing/__about__.py') as fp:
    exec(fp.read(), about)

setup(
    name=NAME,
    version=about["__version__"],
    description=DESCRIPTION,
    author=AUTHOR,
    install_requires=INSTALL_REQUIRES,
    packages=find_packages(),
    url=URL,
    classifiers=CLASSIFIERS
)
